import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

@ccclass
export default class Level29_2 extends LevelBase {

    @property
    speed = 1;

    private _bg1;
    private _bg2;
    private _aborigines;
    private _brink;
    private _wood;
    private _knife;
    private _fx;
    private _tree;

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }
    // -15 -90 0.62
    setStatus(): void {
        this._bg1 = this.otherSprite[0];
        this._bg2 = this.otherSprite[1];
        this._aborigines = this.otherSpine[0];
        this._brink = this.otherSprite[2];
        this._wood = this.otherSprite[3];
        this._knife = this.otherSprite[4];
        this._tree = this.otherSprite[5];
        this._fx = this.otherSpine[1];
        this._fx.node.active = false;
        this._wood.node.active = false;
        this._knife.node.position = cc.v3(185, 122);
        this._knife.node.angle = -7;
        this._knife.node.scaleX = -0.5;
        this._knife.node.scaleY = 0.6;
        this._knife.node.active = false;
        this._wood.node.position = cc.v3(-15, -90);
        this._tree.node.x = 2700;
        this._bg1.node.x = 540;
        this._bg2.node.x = 2700;
        this.lupin.node.scale = 0.8;
        this._aborigines.node.scaleX = -0.8;
        this._aborigines.node.scaleY = 0.8;
        this._brink.node.active = false;
        this.setLupin(cc.v2(-300, -100), "general/run", "emotion/happy_2");
        this.setOtherSpine(this._aborigines, cc.v2(-900, -100), "level_29_1/thodan_chasing", null);
        this.lupin.setMix("general/run", "level_29_1/mc_run_scare", 0.3);
        this.speed = 3;
    }

    setAction(): void {
        let _speed = 10 / this.speed;
        tween(this._bg1.node).to(_speed / 2, {x: -1620})
            .to(0, {x: 2700})
            .repeatForever(
                tween().to(_speed, {x: -1620})
                    .to(0, {x: 2700})
            )
            .start();
        tween(this._bg2.node).repeatForever(
                tween().to(_speed, {x: -1620})
                    .to(0, {x: 2700})
            )
            .start();
        tween(this.lupin.node).to(2, {x: -50})
            .call(() => {
                tween(this._aborigines.node).to(4, {x: -400})
                    .call(() => {
                        this.showOptionContainer(true);
                    })
                    .start();
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_29_1/mc_run_scare", true);
            })
            .to(1.5, {x: 300})
            .start();
        tween(this._tree.node).repeatForever(
                tween().to(_speed, {x: -1620}).to(0, {x: 2700})
            )
            .start();
    }

    runOption1(): void {
        this._aborigines.setMix("level_29_1/thodan_chasing", "level_29_2/soldier_fall", 0.3);
        this._aborigines.setMix("level_29_2/soldier_fall", "level_29_1/thodan_chasing", 0.3);
        this.lupin.setAnimation(0, "level_29_1/mc_run_throwood", false);
        this.scheduleOnce(() => {
            this._wood.node.active = true;
            tween(this._wood.node).by(0.5, {x: -800}).start();
            cc.Tween.stopAllByTarget(this._bg1.node);
            cc.Tween.stopAllByTarget(this._bg2.node);
            cc.Tween.stopAllByTarget(this._tree.node);
            tween(this._bg1.node).by(0.5, {x: -300}, {easing: "cubicOut"})
                .call(() => {
                    this.lupin.node.scaleX = -0.8;
                    this.lupin.setAnimation(0, "emotion/idle", false);
                    this.lupin.setAnimation(1, "emotion/sinister", true);
                }).start();
            tween(this._bg2.node).by(0.5, {x: -300}, {easing: "cubicOut"}).start();
            tween(this._tree.node).by(0.5, {x: -300}, {easing: "cubicOut"}).start();
            this.lupin.setAnimation(0, "level_29_1/mc_run_scare", true);
            this._aborigines.timeScale = 1.2;
            this._aborigines.setAnimation(0, "level_29_2/soldier_fall", false);
            this._aborigines.setCompleteListener(track => {
                if (track.animation.name == "level_29_2/soldier_fall") {
                    this._aborigines.setCompleteListener(null);
                    this.scheduleOnce(() => {
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(0, "general/win_2.1", false);
                        this.lupin.setAnimation(1, "emotion/happy_1", true);
                    }, 1);
                    this.scheduleOnce(() => {
                        this.onPass();
                    }, 3);
                }
            })
        }, 0.69);
    }

    runOption2(): void {
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_29_1/mc_run_throwknife", false);
        this.scheduleOnce(() => {
            this._knife.node.active = true;
            tween(this._knife.node).to(0.2, {position: cc.v3(-350, 270)})
                .call(() => {
                    this._knife.node.active = false;
                }).start();
        }, 0.25);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_29_1/mc_run_throwknife") {
                this.lupin.setCompleteListener(null);
                this.lupin.setAnimation(0, "level_29_1/mc_run_scare", true);
                this._aborigines.setAnimation(0, "level_29_1/thodan_chasing_knive", false);
                this._aborigines.setCompleteListener(track => {
                    if (track.animation.name == "level_29_1/thodan_chasing_knive") {
                        this._aborigines.setCompleteListener(null);
                        this._aborigines.setAnimation(0, "level_29_1/thodan_chasing_knive", true);
                        // this._aborigines.timeScale = 0;
                        this._aborigines.timeScale = 1.2;
                        tween(this._aborigines.node).delay(1.5)
                            .to(3, {x: 100})
                            .call(() => {
                                cc.Tween.stopAllByTarget(this._bg1.node);
                                cc.Tween.stopAllByTarget(this._bg2.node);
                                cc.Tween.stopAllByTarget(this._tree.node);
                                this.showContinue();
                            })
                            .start();
                    }
                })
            }
        })
    }

    runOption3(): void {
        this.lupin.setAnimation(0, "level_29_1/mc_run_teleport", false);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_29_1/mc_run_teleport") {
                this.lupin.setCompleteListener(null);
                this._fx.node.active = true;
                this._fx.timeScale = 1.1;
                this._fx.setAnimation(0, "fx/explosion", false);
                this._fx.setCompleteListener(track => {
                    if (track.animation.name == "fx/explosion") {
                        this._fx.setCompleteListener(null);
                        this._fx.node.active = false;
                        this.speed *= 2;
                        cc.Tween.stopAllByTarget(this._bg1.node);
                        cc.Tween.stopAllByTarget(this._bg2.node);
                        cc.Tween.stopAllByTarget(this._tree.node);
                        tween(this._bg1.node).by(1, {x: -300}, {easing: "cubicOut"}).start();
                        tween(this._bg2.node).by(1, {x: -300}, {easing: "cubicOut"}).start();
                        tween(this._tree.node).by(1, {x: -300}, {easing: "cubicOut"}).start();
                        tween(this._aborigines.node).by(0.3, {x: 300})
                            .call(() => {
                                this._aborigines.setAnimation(0, "level_29_2/soldier_kick", false);
                                this.lupin.node.x = 100;
                                this.lupin.node.opacity = 255;
                                this.lupin.setAnimation(0, "level_29_1/mc_run_scare", true);
                                this.lupin.setMix("level_29_1/mc_run_scare", "general/fall_sml", 0.3);
                                this.scheduleOnce(() => {
                                    this.lupin.setAnimation(0, "general/fall_sml", false);
                                    this.lupin.setAnimation(1, "emotion/fear_2", false);
                                }, 0.3);
                                this._aborigines.setCompleteListener(track => {
                                    if (track.animation.name == "level_29_2/soldier_kick") {
                                        this._aborigines.setCompleteListener(null);
                                        this._aborigines.setAnimation(0, "level_28_3/soldier_lol", true);
                                        this.scheduleOnce(() => {
                                            this.showContinue();
                                        }, 2);
                                    }
                                })
                            })
                            .start();
                    }
                })
                this.lupin.node.opacity = 0;
            }
        })
    }
}
