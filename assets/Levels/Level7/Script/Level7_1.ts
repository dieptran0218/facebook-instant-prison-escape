import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    MUSIC1,
    MUSIC2,
    MUSIC3
  }

@ccclass
export default class Level7_1 extends LevelBase {

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    onDisable(): void {

    }

    initStage() {
        super.initStage();
        this.lupin.clearTrack(2);
        this.otherSpine[0].clearTrack(0);
        this._gameManager.mainCamera.active = false;
        this.lupin.setMix("general/walk_slow", "general/hide1", 0.3);
        this.setLupin(cc.v2(-900, -355), "general/stand_thinking", "general/walk_slow");
        this.setOtherSpine(this.otherSpine[0], cc.v2(-60, -5), null, "police/level_7/book_read");
        this.otherSprite[0].node.active = true;
        this.otherSprite[1].node.active = false;
        tween(this.lupin.node).by(4, {position: cc.v3(330, 0)})
                .call(() => {
                    this.lupin.setAnimation(1, "general/hide1", true);
                    tween(this.node).delay(2).call(() => {
                        this.showOptionContainer(true);
                    }).start();
                }).start();
        this.camera2d[0].position = cc.v3(0, 0);
        this.camera2d[0].active = true;
        this.camera2d[1].active = true;
        this.otherSpine[1].timeScale = 0.5;
    }

    runOption1(): void {
        let isTrue = true;
        this.otherSprite[0].node.active = false;
        this.otherSprite[1].node.active = true;
        this.playSound(SOUND.MUSIC1, false, 0)
        this.otherSpine[1].setAnimation(0, "level_7/gramophone/rock_play", true);
        this.lupin.setAnimation(2, "emotion/excited", true);
        this.lupin.setMix("general/hide1", "general/back", 0.3);
        this.lupin.setMix("general/stand_thinking", "emotion/fear_1", 0.3);
        tween(this.otherSpine[0]).delay(2).call(() => {
                    this.otherSpine[0].clearTrack(0);
                    this.otherSpine[0].setAnimation(0, "police/level_4/gun_raise3", false);
                    tween(this.otherSpine[0].node).delay(0.5)
                            .call(() => {
                                this.otherSpine[0].node.position = cc.v3(-60, -125);
                                this.otherSpine[0].setAnimation(1, "police/level_4/gun_raise3", false);
                            })
                            .start();
                    this.lupin.setAnimation(0, "emotion/fear_1", true);
                    this.lupin.setAnimation(2, "emotion/fear_1", true);
                    this.lupin.setAnimation(1, "general/back", false);
                })
                .start();

        this.otherSpine[0].setCompleteListener(trackEntry => {
            if (trackEntry.animation.name == "police/level_4/gun_raise3" && isTrue)
            {
                isTrue = false;
                this.lupin.timeScale = 0;
                this.otherSpine.forEach(element => {
                    element.timeScale = 0;
                })
                this.showFail(this.selected);
            }
        });
    }

    runOption2(): void {
        let isTrue = true;
        this.playSound(SOUND.MUSIC2, false, 0)
        this.otherSprite[0].node.active = false;
        this.otherSprite[1].node.active = true;
        this.otherSpine[1].setAnimation(0, "level_7/gramophone/ghost_play", true);
        this.lupin.setAnimation(2, "emotion/excited", true);
        this.lupin.setMix("general/hide1", "level_7/fall_giddy", 0.1);
        this.lupin.setMix("general/stand_thinking", "level_7/fall_giddy", 0.1);
        tween(this.otherSpine[0]).delay(2).call(() => {
                    this.otherSpine[0].clearTrack(0);
                    this.otherSpine[0].setAnimation(1, "police/level_7/book_giddy", false);
                    this.lupin.setAnimation(1, "level_7/fall_giddy", false);
                })
                .start();

        this.otherSpine[0].setCompleteListener(trackEntry => {
            console.log("%c" + trackEntry.animation.name, "color: blue");
            if (trackEntry.animation.name == "police/level_7/book_giddy" && isTrue)
            {
                isTrue = false;
                this.lupin.timeScale = 0;
                this.otherSpine.forEach(element => {
                    element.timeScale = 0;
                })
                this.showFail(this.selected);
            }
        });
    }

    runOption3(): void {
        let isTrue = true;
        this.playSound(SOUND.MUSIC3, false, 0)
        this.otherSprite[0].node.active = false;
        this.otherSprite[1].node.active = true;
        this.otherSpine[1].setAnimation(0, "level_7/gramophone/classic_play", true);
        this.lupin.setAnimation(2, "emotion/excited", true);
        this.lupin.setMix("general/hide1", "general/stand_thinking", 0.1);
        tween(this.otherSpine[0]).delay(2).call(() => {
                    this.otherSpine[0].clearTrack(2);
                    this.otherSpine[0].setAnimation(1, "police/level_7/book_sleep", false);
                    tween(this.lupin.node).delay(1).call(() => {
                        this.lupin.setAnimation(0, "emotion/happy_1", false);
                        this.lupin.setAnimation(1, "general/stand_thinking", false);

                    })
                    .start();
                })
                .start();
                
        this.lupin.setCompleteListener(trackEntry => {
            console.log("%c" + trackEntry.animation.name, "color: blue");
            if (trackEntry.animation.name == "emotion/happy_1" && isTrue)
            {
                this.setLupin(cc.v2(this.lupin.node.position), "emotion/whistle", "general/walk");
                tween(this.lupin.node).by(5, {position: cc.v3(1300, 0)})
                        .call(() => {
                            tween(this.node).call(() => {
                                this.onPass();
                            })
                            .start();
                        })
                        .start();
                tween(this.camera2d[0]).by(3, {position: cc.v3(250, 0)}).start();
            }
        });
    }
}
