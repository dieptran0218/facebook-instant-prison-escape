import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND{
    DIG,
    FREEZE,
    PEEK,
    PUNCH,
    SIGH,
    WOOD_BREAK
}

@ccclass
export default class Level7_1 extends LevelBase {

    initStage() {
        super.initStage();

        this._gameManager.mainCamera.active = false;

        this.camera2d[0].position = cc.v3(0, 0);
        this.camera2d[0].active = true;
        this.camera2d[1].active = true;

        this.setLupin(cc.v2(-273, -633), 'emotion/idle', 'general/stand_thinking')
        this.lupin.timeScale = 1
        this.lupin.setCompleteListener(null)

        this.otherSprite[0].node.position = cc.v3(-348, 16)
        this.otherSprite[0].node.angle = 0
        this.otherSprite[0].node.active = true

        this.otherSprite[1].node.active = true
        this.otherSprite[2].node.active = false
        this.otherSprite[3].node.active = false

        this.background.getChildByName('ManySand').active = false

        this.otherSpine[0].node.active = false

        tween(this.node)
            .delay(2)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1() {
        EffectManager.hideScene((node) => {
            let count = 0

            this.setLupin(cc.v2(125, -455), 'emotion/abc', 'general/stand_thinking')
            this.lupin.setMix('general/hammer_break', 'level_9/hammer_tired', .3)
            this.lupin.setCompleteListener((track) => {
                if (track.animation.name === 'general/hammer_break') {
                    count++
                    if (count === 5) {
                        this.lupin.setAnimation(0, 'level_9/hammer_tired', false)
                        this.lupin.setAnimation(1, 'emotion/tired', false)
                        this.playSound(SOUND.SIGH, false, 0)
                        tween(this.node).delay(2).call(() => {
                            this.showContinue()
                        }).start()
                    } else if (count < 5){
                        this.playSound(SOUND.PUNCH, false, 0.7)
                    }
                }
            })

            EffectManager.showScene()
            tween(this.lupin.node)
                .delay(2)
                .call(() => {
                    // cheat to fix arm from stand thinking
                    this.lupin.setAnimation(0, 'general/stand_ready', true)
                    this.playSound(SOUND.PUNCH, false, 0.7)
                    this.lupin.setAnimation(0, 'general/hammer_break', true)
                    this.lupin.setAnimation(1, 'emotion/angry', true)
                })
                .start()

        }, this.node)
    }

    runOption2() {
        EffectManager.hideScene((node) => {
            this.lupin.setMix('general/stand_thinking', 'level_9/climb_ladder', .3)
            this.setLupin(cc.v2(-40, -750), 'emotion/happy_1', 'general/stand_thinking')

            this.lupin.setCompleteListener((track) => {
                if (track.animation.name === 'level_9/climb_ladder') {
                    this.otherSprite[0].node.active = false
                    this.otherSpine[0].node.active = true
                    this.playSound(SOUND.FREEZE, false, 0)
                    this.playSound(SOUND.WOOD_BREAK, false, 1)
                    this.otherSpine[0].setAnimation(0, 'level_9/ladder_break', false)
                    tween(this.lupin.node)
                        .delay(1)
                        .call(() => {
                            this.setLupin(
                                cc.v2(200, -195),
                                'emotion/fear_1',
                                'general/fall_high',
                            )
                        })
                        .delay(2)
                        .call(() => {
                            this.showContinue()
                        })
                        .start()
                }
            })

            this.otherSprite[0].node.position = cc.v3(190, -220)
            this.otherSprite[0].node.angle = -5

            EffectManager.showScene()
            tween(this.lupin.node)
                .delay(2)
                .call(() => {
                    this.lupin.setAnimation(0, 'emotion/idle', true)
                    this.lupin.setAnimation(1, 'level_9/climb_ladder', false)
                })
                .start()

        }, this.node)

    }

    runOption3() {
        EffectManager.hideScene((node) => {
            let count = 0

            this.setLupin(cc.v2(105, -503), 'emotion/abc', 'general/stand_thinking')
            this.lupin.setCompleteListener((track) => {
                if (track.animation.name === 'general/shove_dig') {
                    count++
                    if (count === 2) {
                        this.otherSprite[2].node.active = true

                    } else if (count === 4) {
                        this.background.getChildByName('ManySand').active = true
                        this.playSound(SOUND.PEEK, false, 0)
                        this.otherSprite[1].node.active = false
                        this.otherSprite[2].node.active = false
                        this.otherSprite[3].node.active = true

                        tween(this.lupin.node)
                            .by(0, {position: cc.v3(-100)})
                            .delay(.5)
                            .call(() => {
                                // cheat to fix shove
                                this.lupin.setAnimation(0, 'level_9/surprised_hole', true)
                                this.lupin.setAnimation(1, 'level_9/surprised_hole', true)
                            })
                            .delay(1)
                            .call(() => {
                                this.setLupin(
                                    cc.v2(this.lupin.node.position),
                                    'general/win_1.1',
                                    'emotion/laugh',
                                )
                            })
                            .delay(.5)
                            .call(() => {
                                this.showSuccess(this.selected);
                            })
                            .start()
                    }
                    if (count < 4) {
                        this.playSound(SOUND.DIG, false, 0.7)
                    }
                }
            })

            EffectManager.showScene()
            tween(this.lupin.node)
                .delay(2)
                .call(() => {
                    this.playSound(SOUND.DIG, false, 0.7)
                    this.setLupin(
                        cc.v2(this.lupin.node.position),
                        'emotion/angry',
                        'general/shove_dig',
                    )
                })
                .start()

        }, this.node)
    }
}