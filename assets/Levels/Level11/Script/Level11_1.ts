import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND{
    BLEH_BLEH,
    DIZZY_1,
    DOOR_DROP,
    HEAD_BANG,
    HEY_HEY,
    PULL,
    SIGH,
    THUD,
    CHAIN,
}

@ccclass
export default class Level11_1 extends LevelBase {

    private _bigShark;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
        tween(this.node).delay(1).call(() => {
            this.showOptionContainer(true);
        }).start();
    }

    fishSwimming(): void {

    }

    setStatus(): void {
        cc.Tween.stopAll();
        this.lupin.setMix("general/stand_thinking", "general/walk", 0.3);
        this._bigShark = this.otherSpine[0];
        this.setLupin(cc.v2(-100, -480), "general/stand_thinking", "emotion/abc");
        this._bigShark.node.position = cc.v3(-1200, -600);
        this._bigShark.node.scaleX = 1;
        this._bigShark.setAnimation(0, "swim", true);
        this.camera2d[0].getComponent(cc.Camera).zoomRatio = 1;
        this.background.position = cc.v3()
        this.otherSprite[0].node.position = cc.v3(490, -255);
        this.otherSprite[0].node.angle = 0;
        this.otherSprite[0].node.scaleX = 1;
    }

    setAction(): void {
        tween(this._bigShark.node).repeatForever(
                tween().by(4, {position: cc.v3(2400, 0)})
                    .call(() => {
                        this._bigShark.node.scaleX = -1;
                    })
                    .delay(0.5)
                    .by(4, {position: cc.v3(-2400, 0)})
                    .call(() => {
                        this._bigShark.node.scaleX = 1;
                    })
                    .delay(0.5)
            ).start();
    }

    runOption1(): void {
        this.lupin.setMix("level_11/chain_pull", "level_11/chain_pull", 0.3);
        this.lupin.setMix("level_11/chain_pull", "level_11/chain_pull_fail", 0.3);
        tween(this.lupin.node).call(() => {
                this.lupin.setAnimation(0, "general/walk", true);
                this.lupin.setAnimation(1, "emotion/excited", true);
            })
            .by(0.5, {position: cc.v3(170, 0)})
            .call(() => {
                this.lupin.timeScale = 1.2;
                this.lupin.setAnimation(1, "emotion/angry", false);
                this.lupin.setAnimation(0, "level_11/chain_pull", false);
                this.lupin.addAnimation(0, "level_11/chain_pull", false);
                this.lupin.addAnimation(0, "level_11/chain_pull", false);
                this.lupin.clearTrack(1);
                this.lupin.addAnimation(0, "level_11/chain_pull_fail", false);

                this.playSound(SOUND.PULL, false, .3)

                this.lupin.setStartListener(track => {
                    if (track.animation.name == "level_11/chain_pull_fail") {
                        cc.audioEngine.stopAllEffects()
                        this.playSound(SOUND.CHAIN, false, 0)

                        tween(this.lupin.node)
                            .delay(1.5)
                            .call(() => {
                                this.playSound(SOUND.SIGH, false, 0)
                            })
                            .delay(1.5)
                            .call(() => {
                                this.showFail(this.selected);
                            })
                            .delay(1)
                            .call(() => {
                                cc.Tween.stopAllByTarget(this._bigShark.node);
                            })
                            .start();
                    }
                });
            })
            .start();
    }

    runOption2(): void {
        this.lupin.setMix("general/head_hit_1", "general/head_hit_1", 0.3);
        this.lupin.setMix("general/head_hit_1", "level_7/fall_giddy", 0.3);

        tween(this.lupin.node).call(() => {
                this.lupin.setAnimation(0, "general/walk", true);  
                this.lupin.setAnimation(1, "emotion/excited", true);    
            })
            .by(1, {position: cc.v3(400, 0)})
            .call(() => {
                this.lupin.setAnimation(1, "emotion/angry", true);
                this.lupin.setAnimation(0, "general/head_hit_1", false);
                this.lupin.addAnimation(0, "general/head_hit_1", false);
                this.lupin.addAnimation(0, "general/head_hit_1", false);
                this.lupin.addAnimation(0, "level_7/fall_giddy", false);
                this.playSound(SOUND.HEAD_BANG, false, 0.3)

                let count = 0;
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_7/fall_giddy") {
                        this.showFail(this.selected);
                        cc.Tween.stopAllByTarget(this._bigShark.node);
                    }
                    else if (track.animation.name == "general/head_hit_1") {
                        ++count;

                        count < 3 && this.playSound(SOUND.HEAD_BANG, false, 0)

                        if (count == 3) {
                            this.lupin.setAnimation(1, "level_7/fall_giddy", false);
                            this.playSound(SOUND.DIZZY_1, false, 0)
                        }
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this._bigShark.setMix("swim", "emotion/notice", 0.3);
        this._bigShark.setMix("emotion/notice", "shark_attach", 0.3);
        this._bigShark.setMix("shark_attach", "shark_attach", 0.3);

        this._bigShark.setStartListener((track) => {
            if (track.animation.name === 'shark_attach') {
                this.playSound(SOUND.THUD, false, .3)
            }
        })

        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.lupin.node.scaleX = -1;
                this.lupin.setAnimation(1, "emotion/excited", true);
                cc.Tween.stopAllByTarget(this._bigShark.node);
                this._bigShark.node.scaleX = -1;
                this._bigShark.node.position = cc.v3(205, -600);
                tween(this._bigShark.node).by(2, {position: cc.v3(-1200, 0)})
                    .call(() => {
                        this._bigShark.node.scaleX = 1;  
                    })
                    .by(1, {position: cc.v3(300, 0)}, {easing: "cubicOut"})
                    .call(() => {
                        this._bigShark.setAnimation(0, "emotion/notice", true);
                    })
                    .delay(1)
                    .call(() => {
                        this._bigShark.setAnimation(0, "shark_attach", false);
                        this._bigShark.addAnimation(0, "shark_attach", false);
                        this._bigShark.addAnimation(0, "shark_attach", false);
                    })
                    .delay(0.3)
                    .call(() => {
                        this.vibrate();
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "level_11/shark_joking", true);
                this.lupin.setAnimation(1, "level_11/shark_joking", true);
                this.playSound(SOUND.BLEH_BLEH, true, 0)
            })
            .start();

    }
    vibrate(): void {
        this._bigShark.timeScale = 1.5;
        tween(this.background)
            .repeat(
                15, 
                tween().by(0.05, {position: cc.v3(10, 10)})
                    .by(0.05, {position: cc.v3(-10, -10)})
            )
            .call(() => {
                tween(this.otherSprite[0].node)
                    .call(() => {
                        tween(this.otherSprite[0].node).by(0.5, {position: cc.v3(-260, -175)}).start();
                        tween(this.otherSprite[0].node).call(() => {
                            this.otherSprite[0].node.scaleX = -1;
                        }).start();
                        tween(this.otherSprite[0].node).by(0.5, {angle: 85}).start();

                        this.playSound(SOUND.DOOR_DROP, false, 0.4)
                    })
                    .call(() => {
                        this.lupin.setAnimation(0, "general/win", true);
                        this.lupin.setAnimation(1, "emotion/happy_1", true);
                        this._bigShark.setAnimation(0, "swim", true);
                        tween(this._bigShark.node).by(2, {position: cc.v3(2000, 0)}).start();
                        cc.audioEngine.stopAllEffects()
                        this.playSound(SOUND.HEY_HEY, false , 0)
                    })
                    .delay(2)
                    .call(() => {
                        this.lupin.node.scaleX = 1;
                        this.lupin.setAnimation(0, "general/walk", true);
                        tween(this.lupin.node).call(() => {
                                this.onPass();
                            })
                            .by(1, {position: cc.v3(300, 0)})
                            .call(() => {
                                cc.Tween.stopAllByTarget(this._bigShark.node);
                            })
                            .start();
                    })
                    .start();
            })
            .by(0.05, {position: cc.v3(10, 10)})
            .by(0.05, {position: cc.v3(-10, -10)})
            .call(() => {
                this.background.position = cc.v3(0, 0);
            })
            .start();
    }
}
