import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    BRAWL,
    CROCODILE,
    DOOR_BANG,
    DOOR_SLICE_2,
    GUN_SHIELD,
    KEY,
    WHISTLE
}

@ccclass
export default class Level17_1 extends LevelBase {

    private _crocodile;
    private _door;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }
    
    setStatus(): void {
        this._crocodile = this.otherSpine[0];
        this._door = this.otherSprite[0];
        this.setLupin(cc.v2(-700, -410), "general/walk", "emotion/whistle");
        this._door.node.position = cc.v3(-585, -230);
        this.setOtherSpine(this._crocodile, cc.v2(1000, -410), "run", null);
        this.lupin.setMix("general/walk", "general/stand_nervous", 0.3);
    }

    setAction(): void {
        this.playSound(SOUND.WHISTLE, false, 0);
        tween(this.lupin.node).by(3, {position: cc.v3(400, 0)})
            .call(() => {
                this.lupin.setAnimation(0, "general/stand_nervous", true);
                this.playSound(SOUND.DOOR_SLICE_2, false, 0);
                tween(this._door.node).to(1, {position: cc.v3(-470, -60)}, {easing: "cubicIn"})
                    .call(() => {
                        this.lupin.node.scaleX = -1;
                        this.lupin.setAnimation(1, "emotion/fear_1", true);
                        this.playSound(SOUND.CROCODILE, true, 0);
                        tween(this._crocodile.node).by(3, {position: cc.v3(-485, 0)})
                            .call(() => {
                                this._crocodile.setAnimation(0, "angry", true);
                            })
                            .delay(1)
                            .call(() => {
                                this.showOptionContainer(true);
                            }).start();
                    })
                    .delay(1)
                    .call(() => {
                        this.lupin.node.scaleX = 1;
                        this.lupin.setAnimation(1, "emotion/fear_2", true);
                    }).start();
            })
            .start();
    }

    runOption1(): void {
        cc.audioEngine.stopAllEffects();
        this.lupin.setMix("level_17/shoot_gun", "general/back", 0.3);
        this._crocodile.setMix("run", "angry", 0.3);
        this._crocodile.setMix("angry", "using_shield", 0.3);
        this._crocodile.setMix("using_shield", "angry", 0.3);
        this.lupin.setAnimation(1, "emotion/sinister", true);
        tween(this.lupin.node).delay(2)
            .call(() => {
                this.lupin.timeScale = 1.5;
                this._crocodile.timeScale = 2;
                this.playSound(SOUND.GUN_SHIELD, true, 0);
                this.playSound(SOUND.GUN_SHIELD, true, 0.3);
                this.playSound(SOUND.GUN_SHIELD, true, 0.6);
                this.lupin.setAnimation(0, "level_17/shoot_gun", true);
                this._crocodile.setAnimation(0, "using_shield", false);
                this._crocodile.setCompleteListener(track => {
                    if (track.animation.name == "using_shield") {
                        this._crocodile.setCompleteListener(null);
                        this._crocodile.timeScale = 1.5;
                    }
                })
                this._crocodile.addAnimation(0, "using_shield2", true);
            })
            .delay(5)
            .call(() => {
                cc.audioEngine.stopAllEffects();
                this.playSound(SOUND.CROCODILE, false, 0);
                this.lupin.timeScale = 1;
                this.lupin.setAnimation(0, "general/back", false);
                this.lupin.setAnimation(1, "emotion/fear_2", true);
                this._crocodile.setAnimation(0, "angry", true);
            })
            .delay(1)
            .call(() => {
                this.showFail(this.selected);
            })
            .start();
    }

    runOption2(): void {
        this._crocodile.setMix("angry", "using_key", 0.3);
        this.lupin.node.scaleX = -1;
        this.lupin.timeScale = 2;
        this.playSound(SOUND.DOOR_BANG, true, 0);
        this.playSound(SOUND.DOOR_BANG, true, 0.4);
        this.lupin.setAnimation(0, "level_17/hand_door", true);
        this.lupin.setAnimation(1, "emotion/fear_1", true);
        tween(this._crocodile.node).delay(3)
            .call(() => {
                let count = 0;
                this.playSound(SOUND.KEY, false, 0);
                this._crocodile.setAnimation(0, "using_key", true);
                this._crocodile.setCompleteListener(track => {
                    if (track.animation.name == "using_key") {
                        ++count;
                        if (count == 5) {
                            this._crocodile.setCompleteListener(null);
                            this.lupin.node.scaleX = 1;
                            this.lupin.timeScale = 1;
                            cc.audioEngine.stopAllEffects();
                            this.lupin.setAnimation(0, "general/stand_nervous", true);
                            this.lupin.setAnimation(1, "emotion/fear_2", true);
                            tween(this.node).delay(1)
                                .call(() => {
                                    this.showFail(this.selected);
                                })
                                .start();
                        }
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setMix("general/stand_nervous", "general/stand_ready", 0.3);
        this.lupin.setMix("general/stand_ready", "general/run", 0.3);
        this.lupin.setAnimation(1, "emotion/angry", true);
        this.lupin.setAnimation(0, "general/stand_ready", true);
        tween(this.lupin.node).delay(1.5)
            .call(() => {
                this.lupin.setAnimation(0, "general/run", true);
                tween(this.lupin.node).to(0.75, {position: cc.v3(50, -410)})
                    .call(() => {
                        this._crocodile.node.active = false;
                        this.lupin.setAnimation(1, "emotion/worry", false);
                        this.lupin.clearTrack(1);
                        this.lupin.node.scale = 1.5;
                        cc.audioEngine.stopAllEffects();
                        this.playSound(SOUND.BRAWL, false, 0);
                        this.lupin.setAnimation(0, "fx/fightcloud", true);
                        this.lupin.node.position = cc.v3(this.lupin.node.x + 150, this.lupin.node.y);
                        tween(this.node).delay(3.5)
                            .call(() => {
                                this._crocodile.node.active = true;
                                this._crocodile.setAnimation(0, "strangled", true);
                                this._crocodile.node.position = cc.v3(300, -410);
                                this.lupin.node.scale = 1;
                                this.lupin.setAnimation(0, "level_17/strangle", true);
                                this.lupin.node.position = cc.v3(420, -340);
                            })
                            .delay(2)
                            .call(() => {
                                this.onPass();
                            })
                            .start();

                    }).start();
            })
            .start();
    }
}
