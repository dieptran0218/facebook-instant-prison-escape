import LevelBase from "../../../Scripts/LevelBase";

enum SOUND {
    OWL,
    GUN,
    MACHINE_GUN,
    BARK_FAKE,
    ALERT
}
const {ccclass, property} = cc._decorator;
const tween = cc.tween;
@ccclass
export default class Level10_2 extends LevelBase {

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }

    setStatus(): void {
        this.otherSpine[0].setMix("level_10/lighting1", "level_10/lighting2", 0.3);
        this.otherSpine[0].setMix("level_10/lighting1", "level_10/lighting3", 0.3);
        this.setLupin(cc.v2(-855, -590), "general/stand_nervous", "emotion/nervous");
        this.camera2d[0].position = cc.v3(250, 0);
        this.otherSpine[0].setAnimation(0, "level_10/lighting1", true);
        this.otherSpine[1].node.active = false;
        this.otherSpine[2].node.active = false;
        this.background.position = cc.v3(1080, 0);
    }

    setAction(): void {
        this.playSound(SOUND.OWL, false, 1)
        tween(this.camera2d[0]).by(3, {position: cc.v3(1750, 0)})
            .delay(1)
            .by(2, {position: cc.v3(-1750, 0)}, {easing: "cubicOut"})
            .call(() => {
                tween(this.node).delay(2).call(() => {
                    this.showOptionContainer(true);
                }).start();
            })
            .start();
    }

    runOption1(): void {
        this.lupin.setMix("general/hide3", "level_10/dog_gaugau", 0.3);
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                let true1 = true;
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "general/hide3", false);
                this.lupin.addAnimation(0, "level_10/dog_gaugau", false);
                this.lupin.setStartListener( track => {
                    if (track.animation.name == "level_10/dog_gaugau" && true1)
                    {
                        this.playSound(SOUND.BARK_FAKE, false, 0.2)
                    }
                })
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_10/dog_gaugau" && true1)
                    {
                        true1 = false;
                        tween(this.lupin.node).delay(1)
                            .call(() => {
                                this.lupin.setAnimation(0, "level_10/dog_walk", true);
                            }).by(4, {position: cc.v3(1500, 0)})
                            .call(() => {
                                this.onPass();
                            }).start();
                    }
                });
                this.lupin.node.position = cc.v3(200, -590);
                this.otherSpine[0].setAnimation(0, "level_10/lighting3", false);
                this.camera2d[0].position = cc.v3(1600, 0);
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        this.lupin.setMix("general/hide3", "level_10/gun_shot", 0.3);
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                let true1 = true;
                this.lupin.node.position = cc.v3(200, -590);
                this.otherSpine[0].setAnimation(0, "level_10/lighting2", false);
                this.camera2d[0].position = cc.v3(1600, 0);
                this.lupin.clearTrack(1);
                this.lupin.timeScale = 1.5;
                this.lupin.setAnimation(0, "general/hide3", false);
                this.lupin.addAnimation(0, "level_10/gun_shot", true);
                this.playSound(SOUND.GUN, false, 3)
                this.playSound(SOUND.GUN, false, 4)
                this.playSound(SOUND.GUN, false, 4.5)
                this.playSound(SOUND.ALERT, false, 5)
                this.playSound(SOUND.MACHINE_GUN, false, 7)
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_10/gun_shot" && true1)
                    {
                        true1 = false;
                        tween(this.lupin.node).delay(3)
                            .call(() => {
                                this.otherSpine[3].timeScale = 1.6;
                                this.otherSpine[4].timeScale = 1.8;
                                this.otherSpine[5].timeScale = 2.1;
                                this.otherSpine[1].node.active = true;
                                this.otherSpine[2].node.active = true;
                                this.lupin.setAnimation(0, "general/back", false);
                                this.lupin.setAnimation(1, "emotion/fear_1", false);
                            })
                            .delay(3)
                            .call(() => {
                                this.otherSpine[3].timeScale = 0;
                                this.otherSpine[4].timeScale = 0;
                                this.otherSpine[5].timeScale = 0;
                                this.showContinue();
                            }).start();
                    }
                });
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption3(): void {
        this.lupin.setMix("general/hide3", "general/back", 0.3);
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                let true1 = true;
                this.lupin.node.position = cc.v3(200, -590);
                this.otherSpine[0].setAnimation(0, "level_10/lighting2", false);
                this.camera2d[0].position = cc.v3(1600, 0);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "general/hide3", false);
                this.playSound(SOUND.ALERT, false, 4.5)
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/hide3" && true1)
                    {
                        true1 = false;
                        tween(this.node).delay(0.3)
                            .call(() => {
                                this.lupin.setAnimation(0, "general/back", true);
                                this.lupin.setAnimation(1, "emotion/fear_2", true);
                                tween(this.node).delay(1).call(() => {
                                    this.showContinue();
                                }).start();
                            })
                            .start();
                    }
                })
            })
            .to(0.5, {opacity: 0})
            .start();
    }
}
