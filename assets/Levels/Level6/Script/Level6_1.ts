import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

@ccclass
export default class Level6_1 extends LevelBase {

    onEnable(): void {
        super.onEnable();
        this._gameManager._currentScene = this.background;
        this.background.active = true;
        this.lupin.setAnimation(3, "emotion/idle", true);
        this.setLupin(cc.v2(0, -355), "general/stand_nervous", "general/stand_thinking");
        this.lupin.node.active = true;
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    runOption1(): void {
        this.startOption();
    }

    runOption2(): void {
        this.startOption();
    }

    runOption3(): void {
        this.startOption();
    }

    startOption(): void {
        tween(this.shadow).to(0.3, {opacity: 255})
        .call(() => {
            EffectManager.hideScene(EventManager.sendRequestNextScene, this.node);
        })
        .start();
    }

    initStage() : void {
        super.initStage();
        this.background.active = true;
        this._gameManager._currentScene = this.background;
        tween(this.node).delay(2).call(() => {
            this.showOptionContainer(true);
        }).start();
    }
}
