import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

enum SOUND {
    EXPLOSION,
    SNEAK
  }
const {ccclass, property} = cc._decorator;
const tween = cc.tween;

@ccclass
export default class Level6Option2_1 extends LevelBase {

    onEnable(): void {
        this.lupin.setSkin('Lupin')
        tween(this.shadow).to(0.3, {opacity: 0}).start();
        this.runAction();
        this.lupin.timeScale = 1;
        this.otherSpine.forEach(spine => {
            spine.timeScale = 1;
        });
        this.selected = 2;
    }

    runAction(): void {
        var isTrue = true;
        this.setLupin(cc.v2(-680, -355), "emotion/worry", "general/walk_slow");
        this.playSound(SOUND.SNEAK, false, 0.1)
        tween(this.lupin.node).by(5, {position: cc.v3(400, 0)})
                    .call(() => {
                        this.lupin.setMix("general/walk_slow", "general/stand_thinking", 0.3);
                        this.lupin.setAnimation(1, "general/stand_thinking", false);
                        this.lupin.setAnimation(0, "emotion/excited", true);
                    })
                    .delay(2)
                    .call(() => {
                        this.lupin.setMix("general/stand_thinking", "general/walk", 0.3);
                        this.lupin.setAnimation(1, "general/walk", true);
                        this.lupin.setAnimation(0, "emotion/sinister", true);
                    })
                    .by(3, {position: cc.v3(250, 0)})
                    .call(() => {
                        this.playSound(SOUND.EXPLOSION, false, 0.1)
                        this.lupin.setAnimation(1, "fx/explosive2", false);
                        this.lupin.setCompleteListener(trackEntry => {
                            if (trackEntry.animation.name == "fx/explosive2" && isTrue)
                            {
                                isTrue = false;
                                tween(this.node).call(() => {
                                    EffectManager.showX(this.selected, this.getLineCurrent());
                                })
                                .delay(1)
                                .call(function() {
                                    this.lupin.timeScale = 0;
                                    this.otherSpine.forEach(police => {
                                        police.timeScale = 0;
                                    });
                                    EffectManager.effectFail();
                                    EffectManager.showUI(false);
                                }.bind(this))
                                .call(() => {
                                    this.showFail(this.selected);
                                })
                                .start();
                            }
                        })
                    })
                    .start();
    }
}

