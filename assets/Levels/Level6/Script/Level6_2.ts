import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

 enum SOUND {
    GHOST,
    POKE,
    SPRAY,
    SWOOSH,
    DIZZY
  }

@ccclass
export default class Level6_2 extends LevelBase {

    onEnable():void {
        super.onEnable();
        this._gameManager.mainCamera.active = false;
        this.initStage();
    }

    onDisable():void {
        super.onDisable();
        this._gameManager.mainCamera.active = true;
        this.camera2d.forEach(camera => {
            camera.active = false;
        });
    }

    initStage(): void {
        super.initStage();
        this.lupin.node.scaleX = 0.8;
        this.lupin.node.active = true;
        this.otherSpine[0].node.active = true;
        this.background.position = cc.v3(210, 0);
        this.setLupin(cc.v2(-380, -355), "emotion/idle", "general/stand_nervous");
        this.setOtherSpine(this.otherSpine[0], cc.v2(20, -142), null, "police/level_6/sit_work");
        tween(this.node).delay(2).call(() => {
            this.showOptionContainer(true);
        }).start();
    }

    runOption1(): void {
        var isTrue = true;
        this.camera2d[0].active = true;
        tween(this.lupin.node).delay(1)
                    .call(() => {
                        this
                        this.lupin.setAnimation(0, "fx/explosion", false);
                        this.lupin.setCompleteListener(trackEntry => {
                            if (trackEntry.animation.name == "level_9/door_creep")
                            {
                                this.onPass();
                            }
                            else if (trackEntry.animation.name == "level_6/spray_sleep")
                            {
                                cc.Tween.stopAllByTarget(this.lupin);

                                this.scheduleOnce(() => {
                                    this.lupin.node.scaleX = 0.8;
                                    this.lupin.setAnimation(1, "level_6/doctor_fake", true);
                                    tween(this.camera2d[0]).by(3, {position: cc.v3(400, 0)}).start();
                                    tween(this.lupin.node).by(4, {position: cc.v3(575, -185)}).
                                        call(() => {
                                            this.lupin.setAnimation(1, "level_9/door_creep", false);
                                        }).start();
                                }, 2)
                                
                            }
                            else if (trackEntry.animation.name == "fx/explosion" && isTrue)
                            {
                                isTrue  = false;
                                this.lupin.addAnimation(0, "level_6/doctor_fake", true);
                                this.setLupin(cc.v2(-380, -355), "level_6/doctor_fake", "level_6/doctor_fake");
                                tween(this.lupin.node).by(4, {position: cc.v3(625, 185)})
                                            .call(() => {
                                                this.lupin.clearTrack(0);
                                                this.playSound(SOUND.SPRAY, false, 0.5)
                                                this.lupin.setAnimation(1, "level_6/spray_sleep", false);
                                                this.lupin.node.scaleX = -0.8;
                                            })
                                            .start();
                            }
                        });
                        this.lupin.setStartListener(trackEntry => {
                            if (trackEntry.animation.name == "level_6/spray_sleep")
                            {
                                tween(this.lupin.node).delay(1).call(() => {
                                        this.playSound(SOUND.DIZZY, false, 0)
                                        this.otherSpine[0].setAnimation(1, "police/level_6/go_sleep", false);
                                    })
                                    .start();
                            }
                        })
                    })
                    .start();
    }

    runOption2():void {
        var isTrue = true;
        tween(this.lupin.node).delay(1)
                    .call(() => {
                        this.lupin.setAnimation(0, "fx/explosion", false);
                        this.lupin.setCompleteListener(trackEntry => {
                            if (trackEntry.animation.name == "fx/explosion" && isTrue)
                            {
                                isTrue  = false;
                                this.lupin.addAnimation(0, "level_6/doctor_fake", true);
                                this.setLupin(cc.v2(-380, -355), "level_6/doctor_fake", "level_6/doctor_fake");
                                tween(this.lupin.node).by(3, {position: cc.v3(420, 0)})
                                            .call(() => {
                                                this.playSound(SOUND.SWOOSH, false, 1)
                                                this.otherSpine[0].setAnimation(1, "police/level_6/wave_medicine", false);
                                                this.otherSpine[0].setCompleteListener(trackEntry => {
                                                    if (trackEntry.animation.name == "police/level_6/wave_medicine")
                                                    {
                                                        tween(this.node).delay(1).call(() => {
                                                            this.lupin.timeScale = 0;
                                                            this.otherSpine.forEach(spine => {
                                                                spine.timeScale = 0;
                                                            })
                                                            tween(this.node).call(() => {
                                                                this.showContinue();
                                                            })
                                                            .start();
                                                        })
                                                        .start();;
                                                    }
                                                })
                                            })
                                            .by(2, {position: cc.v3(280, 0)})
                                            .call(() => {
                                                this.lupin.node.scaleX = -0.8;
                                                this.lupin.setAnimation(1, "level_6/doctor_fake_stop", false);
                                            })
                                            .start();
                                // tween(this.lupin)
                            }
                        })
                    })
                    .start();
    }

    runOption3():void {
        var isTrue = true;
        tween(this.shadow).to(0.3, {opacity: 255})
                .call(() => {
                    this.playSound(SOUND.GHOST,false,0)
                    this.lupin.setAnimation(1, "general/ghost_fake", true);
                    tween(this.lupin.node).repeat(2, 
                                    tween().by(1, {position: cc.v3(200, 60)}).delay(1.3)
                                )
                                .by(1, {position: cc.v3(200, 60)})
                                .delay(1)
                                .call(() => {
                                    this.lupin.setAnimation(1, "general/ghost_fake1", false);
                                    this.scheduleOnce(()=>{
                                        cc.audioEngine.stopAllEffects()
                                        this.playSound(SOUND.POKE,false,0)
                                    },1)
                                    this.otherSpine[0].setAnimation(1, "police/level_6/needle_put", false);
                                    this.otherSpine[0].setCompleteListener(trackEntry => {
                                        if (trackEntry.animation.name == "police/level_6/needle_put" && isTrue)
                                        {
                                            isTrue = false;
                                            tween(this.node).delay(1).call(() => {
                                                this.lupin.timeScale = 0;
                                                this.otherSpine.forEach(spine => {
                                                    spine.timeScale = 0;
                                                })
                                                tween(this.node).call(() => {
                                                    this.showContinue();
                                                })
                                                .start();
                                            })
                                            .start();;
                                        }
                                    })
                                })
                                .start();
                })
                .delay(0.3)
                .to(0.3, {opacity: 0})
                .start();
    }
}

