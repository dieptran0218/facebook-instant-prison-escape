import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween =cc.tween;
enum SOUND {
    BEEP,
    MUSIC_OPTION_1,
    PULL,
    SCREAM,
    WATER_SPLASH,
    BITE,
    QUACK,
    THROW,
}

@ccclass
export default class Level20_3 extends LevelBase {

    private _shark1;
    private _shark2;
    private _otherShark;
    private _cano;

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }

    setStatus(): void {
        this._shark1 = this.otherSpine[0];
        this._shark2 = this.otherSpine[1];
        this._otherShark = this.otherSprite[0];
        this._otherShark.position = cc.v3(700, -575);
        this._otherShark.node.scaleX = -0.5;
        this._shark2.node.zIndex = 0;
        this.background.y = 100;
        this.lupin.node.zIndex = 99;
        this.setLupin(cc.v2(-370, -600), "general/sit", "emotion/happy_1");
        this._shark1.node.position = cc.v3(785, -755);
        this._shark2.node.position = cc.v3(1140, -740);
        this._shark2.node.scaleX = -0.8;
        this._cano = this.otherSpine[2];
        this.otherSprite[1].node.zIndex = cc.macro.MAX_ZINDEX;
        this._cano.node.zIndex = 100;
        this._cano.setAnimation(0, "level_19/cano_idle", true);
        this._shark1.setAnimation(0, "shark_move", true);
        this._shark2.setAnimation(0, "shark_move", true);
        this.lupin.setMix("general/sit", "general/stand_nervous", 0.3);
        this._shark1.node.scaleX = -0.8;
    }

    setAction(): void {
        tween(this._shark1.node).to(2, {position: cc.v3(-50, -755)}).start();
        tween(this._shark2.node).to(2, {position: cc.v3(285, -740)}).start();
        tween(this._otherShark.node).to(1.5, {position: cc.v3(265, -575)})
            .call(() => {
                this.lupin.node.position = cc.v3(-370, -520);
                this.lupin.setAnimation(0, "general/stand_nervous", true);
                this.lupin.setAnimation(1, "emotion/abc", true);

                this.playSound(SOUND.SCREAM, false, 0)

                this.showOptionContainer(true);
            }).start();

    }

    runOption1(): void {
        this.lupin.setAnimation(0, "level_20_3/mc_time_machine", false);

        this.playSound(SOUND.BEEP, false, .8)

        tween(this.node).delay(1)
            .call(() => {
                this.lupin.setAnimation(1, "emotion/happy_2", true);
                this._shark2.setAnimation(0, "shark_tranform_1", false);
                this._shark2.node.zIndex = 101;
                this._shark1.setAnimation(0, "shark_tranform_2_idle", false);

                this.playSound(SOUND.QUACK, false, .5)

                tween(this._shark2.node).to(0.7, {position: cc.v3(5, -370)})
                    .delay(0.5)
                    .to(0.5, {position: cc.v3(-100, -600)})
                    .delay(2)
                    .call(() => {
                        this._shark1.node.scaleX = 0.8;
                        this._shark2.node.scaleX = 0.8;
                        this._otherShark.node.scaleX = 0.5;
                        tween(this._shark1.node).by(2, {position: cc.v3(1000, 0)})
                            .call(() => {
                                this.onPass();
                            }).start();
                        tween(this._shark2.node).by(2, {position: cc.v3(1000, 0)}).start();
                        tween(this._otherShark.node).by(2, {position: cc.v3(1000, 0)}).start();
                    }).start();

            })
            .start();
    }

    runOption2(): void {
        this.lupin.setMix("level_20_3/mc_fishing_1", "level_20_3/mc_fishing_2", 0.3);
        this.lupin.setMix("level_20_3/mc_fishing_2", "level_20_3/mc_fishing_3", 0.3);
        this.lupin.setAnimation(0, "level_20_3/mc_fishing_1", false);
        this.lupin.setAnimation(1, "emotion/sinister", true);

        this.playSound(SOUND.THROW, false, .2)

        tween(this._shark2.node).call(() => {
                this._shark2.node.scaleX = 0.8;
                this._shark2.timeScale = 1.5;
                this._shark2.setAnimation(0, "shark_move", true);
            })
            .by(1, {position: cc.v3(300, 0)})
            .call(() => {
                this.lupin.setAnimation(0, "level_20_3/mc_fishing_2", false);
                this.lupin.setAnimation(1, "emotion/angry", true);

                this.playSound(SOUND.PULL, false, 0)

                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_20_3/mc_fishing_2") {
                        this.lupin.setCompleteListener(null);
                        tween(this._shark2.node).by(1, {position: cc.v3(600, 0)}).start();
                        this.lupin.node.zIndex = 101;
                        this.lupin.setAnimation(0, "level_20_3/mc_fishing_3", false);
                        this.lupin.setAnimation(1, "emotion/fear_2", true);

                        cc.audioEngine.stopAllEffects()
                        this.playSound(SOUND.WATER_SPLASH, false, 0)

                        tween(this.lupin.node).delay(0.3).by(1, {position: cc.v3(600, 0)})
                                    .call(() => {
                                        this.showContinue();
                                    })
                                    .start();
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setAnimation(0, "level_20_3/mc_radio_dance_1", false);
        this.lupin.addAnimation(0, "level_20_3/mc_radio_dance_2", true);

        this.lupin.setStartListener(track => {
            if (track.animation.name == "level_20_3/mc_radio_dance_2") {
                this.lupin.setStartListener(null);
                this.lupin.setAnimation(1, "emotion/happy_2", true);

                this.playSound(SOUND.MUSIC_OPTION_1, false, 0)
            }
        });

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_20_3/mc_radio_dance_2") {
                this._shark1.node.zIndex = 101;
                this._shark1.setAnimation(0, "shark_attack", false);
                // this._shark1.addAnimation(0, "shark_move", true);

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.BITE, false, 0)

                this._shark1.setCompleteListener(track => {
                    if (track.animation.name == "shark_attack") {
                        this._shark1.timeScale = 0;
                        tween(this._shark1.node).by(1, {position: cc.v3(-1000, -500)}).start();
                    }
                });

                tween(this.node).delay(0.5)
                    .call(() => {
                        this.lupin.setAnimation(0, "general/fall_high", true);
                        this.lupin.setAnimation(1, "emotion/fear_2", true);
                        tween(this.lupin.node).by(2, {position: cc.v3(0, -500)}, {easing: "easeInOutQuad"})
                            .call(() => {
                                this.showContinue();
                            }).start();
                            this._cano.timeScale = 0.7;
                        this._cano.setAnimation(0, "level_20_3/cano_shink", false);
                        this._cano.addAnimation(0, "level_20_3/cano_shink_idle", false);

                        this.playSound(SOUND.WATER_SPLASH, false, 0)
                    })
                    .start();
            }
        })
    }
}
