import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

@ccclass
export default class Level28_2 extends LevelBase {

    @property(cc.SpriteFrame)
    bgFrames: cc.SpriteFrame[] = []

    @property(cc.Mask)
    otherMasks: cc.Mask[] = []

    initStage(): void {
        super.initStage()
        this.setStatus()
        this.setAction()
    }

    setStatus(): void {
        this.lupin.setToSetupPose()
        this.setLupin(cc.v2(183, -105), 'level_28_3/mc_looking_around', null)
        this.lupin.node.scaleX = -.5
        this.lupin.node.scaleY = .5
        this.lupin.setCompleteListener(null)

        this.setBGFrame(0)
        this.background.scale = 2

        this.otherSpine[0].node.active = false
        this.setOtherSpine(this.otherSpine[0], cc.v2(312, -177), 'level_28_3/soldier_cot_de', null)
        this.otherSpine[0].timeScale = 0
        this.otherSpine[0].setCompleteListener(null)

        this.otherSprite[0].node.active = true
        this.otherSprite[1].node.active = false

        this.otherSprite[2].node.active = false
        this.otherSprite[2].node.position = cc.v3(-206, 427)
        this.otherSprite[2].node.angle = 0
        this.otherSprite[2].node.scaleX = 0.66

        this.otherMasks[0].node.active = false
        this.otherMasks[1].node.active = false
    }

    setAction(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/mc_got_hit') {
                this.lupin.setCompleteListener(null)

                EffectManager.hideScene((node) => {
                    this.setBGFrame(1)
                    this.background.scale = 1

                    this.lupin.node.scale = 1
                    this.lupin.setToSetupPose()
                    this.lupin.setAnimation(0, 'level_28_3/mc_tie_idle', true)
                    this.lupin.node.position = cc.v3(-107, -265)

                    this.otherSpine[0].node.active = true

                    this.otherSprite[0].node.active = false
                    this.otherSprite[1].node.active = true

                    EffectManager.showScene()

                    this.scheduleOnce(() => {
                        this.showOptionContainer(true)
                    }, 1)
                }, this.node)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_28_3/mc_got_hit', false)
        }, 2)
    }

    setBGFrame(index) {
        this.background.getComponent(cc.Sprite).spriteFrame = this.bgFrames[index]
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/mc_give_money') {
                this.otherSpine[0].timeScale = 1

                this.otherSpine[0].setAnimation(0, 'level_28_3/soldier_lol', false)
                this.otherSpine[0].addAnimation(0, 'level_28_3/soldier_hit', false)
                this.otherSpine[0].addAnimation(0, 'level_28_3/soldier_lol', true)
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/soldier_lol') {
                this.otherSpine[0].setCompleteListener(null)

                this.lupin.setAnimation(0, 'level_28_3/mc_get_hit', false)
                this.lupin.addAnimation(0, 'level_28_3/mc_get_hit_idle', true)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 3)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_28_3/mc_give_money', false)
        }, 1)
    }

    runOption2(): void {
        let count = 0

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/mc_break_out') {
                this.setLupin(cc.v2(141, -128), 'fx/fightcloud', null)
                this.otherSpine[0].node.opacity = 0
                this.otherSpine[0].timeScale = 1
                this.otherSpine[0].setAnimation(0, 'level_28_3/soldier_cot_de', false)
            }

            if (track.animation.name === 'general/win2') {
                count++

                if (count > 1) {
                    this.lupin.setAnimation(0, 'general/walk', true)
                    tween(this.lupin.node)
                        .to(1.5, {position: cc.v3(245, -37)})
                        .call(() => {
                            this.lupin.clearTrack(1)
                            this.lupin.setAnimation(0, 'level_28_3/mc_looking_around', false)
                            this.lupin.addAnimation(0, 'level_28_3/mc_got_hit', false)
                        })
                        .delay(4)
                        .call(() => {
                            this.showContinue()
                        })
                        .start()
                }
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/soldier_cot_de') {
                this.setLupin(cc.v2(-63, -283), 'general/win2', null)
                this.otherSpine[0].node.opacity = 255
            }
        })

        this.scheduleOnce(() => {
            this.otherSprite[2].node.active = true
            this.lupin.setAnimation(0, 'level_28_3/mc_break_out', false)
            this.lupin.addAnimation(0, 'fx/fightcloud', true)
        }, 1)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/mc_break_out2') {
                this.otherSprite[2].node.position = cc.v3(476, -218)
                this.otherSprite[2].node.scaleX = -0.66
                this.otherSprite[2].node.angle = -90
                this.otherSprite[2].node.active = true
            }
        })

        tween(this.lupin.node)
            .delay(1)
            .flipX()
            .to(0, {position: cc.v3(-306, -265)})
            .delay(.5)
            .flipX()
            .to(0, {position: cc.v3(-107, -265)})
            .delay(1)
            .flipX()
            .to(0, {position: cc.v3(-306, -265)})
            .delay(2)
            .flipX()
            .to(0, {position: cc.v3(-107, -265)})
            .delay(.5)
            .flipX()
            .to(0, {position: cc.v3(-306, -265)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_28_3/mc_break_out2', false)
                this.lupin.addAnimation(0, 'general/celebrate_ronaldo2', true)

                this.otherSpine[0].timeScale = 1
                this.otherSpine[0].setAnimation(0, 'level_28_3/soldier_cot_de', false)
            })
            .delay(4)
            .flipX()
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/fear_2')
                this.otherMasks[0].node.active = true
                this.otherMasks[1].node.active = true
            })
            .to(3, {position: cc.v3(367, 103)})
            .call(() => {
                this.showSuccess()
            })
            .start()
    }

}
