import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

@ccclass
export default class Level28_2 extends LevelBase {

    @property(cc.SpriteFrame)
    bgFrames: cc.SpriteFrame[] = []

    initStage(): void {
        super.initStage()
        this.setStatus()
        this.setAction()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-672, 118), 'general/walk_slow', 'emotion/tired')
        this.lupin.node.scale = .6
        this.lupin.node.active = true

        this.setBGFrame(0)

        this.otherSpine[0].node.active = false
        this.otherSpine[0].node.position = cc.v3(-126, -68)

        this.otherSpine[1].node.active = false
        this.otherSpine[1].setAnimation(0, 'level_26_2/mc_stone', false)

        this.otherSpine[3].node.active = false
        this.otherSpine[3].node.position = cc.v3(778, 345)

        this.otherSprite[0].node.active = false
        this.otherSprite[1].node.active = false

        this.otherSprite[2].node.active = true
        this.otherSprite[2].node.scale = 3
        this.otherSprite[2].node.position = cc.v3(127, -389)

        this.otherSprite[3].node.active = true
        this.otherSprite[3].node.position = cc.v3(-751, 652)

        this.otherSprite[4].node.active = false
        this.otherSprite[5].node.active = false

        this.otherSprite[6].node.active = false
        this.otherSprite[6].node.position = cc.v3(371, 358)
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(5, {position: cc.v3(-296, 118)})
            .flipX()
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node), 'general/stand_nervous', 'emotion/fear_2')
            })
            .delay(1)
            .call(() => {
                EffectManager.hideScene((node) => {
                    this.lupin.node.position = cc.v3(-117, -304)
                    this.lupin.node.scaleX = .6

                    this.setBGFrame(2)

                    this.otherSprite[0].node.active = true
                    this.otherSprite[1].node.active = true
                    this.otherSprite[3].node.active = false

                    this.otherSprite[2].node.scale = 1
                    this.otherSprite[2].node.position = cc.v3(188, -565)

                    EffectManager.showScene()


                }, this.node)
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.otherSprite[3].node)
            .delay(4.5)
            .to(.5, {position: cc.v3(-470, 267)})
            .start()
    }

    setBGFrame(index) {
        this.background.getComponent(cc.Sprite).spriteFrame = this.bgFrames[index]
    }

    hideSpriteMainSence() {
        this.otherSprite[0].node.active = false
        this.otherSprite[1].node.active = false
        this.otherSprite[2].node.active = false
    }

    runOption1(): void {
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, 'level_1/concrete_drilling', false)
        this.lupin.setAnimation(1, 'emotion/angry', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_1/concrete_drilling') {
                this.lupin.setAnimation(0, 'level_1/concrete_drilling2', false)
                this.lupin.setAnimation(1, 'emotion/idle', true)
            }

            if (track.animation.name === 'level_1/concrete_drilling2') {
                EffectManager.hideScene((node) => {
                    this.setBGFrame(1)
                    this.background.position = cc.v3(434, 0)

                    this.hideSpriteMainSence()
    
                    this.lupin.node.position = cc.v3(-724, 1074)
                    this.lupin.node.scale = 1
                    this.lupin.timeScale = 0
                    this.lupin.setAnimation(0, 'level_1/concrete_drilling', false)

                    EffectManager.showScene()

                    tween(this.lupin.node)
                        .to(1.5, {position: cc.v3(-604, -221)})
                        .call(() => {
                            this.lupin.timeScale = 1
                            this.lupin.clearTrack(1)
                            this.lupin.setAnimation(0, 'general/celebrate_ronaldo2', true)
                        })
                        .delay(3)
                        .call(() => {
                            this.setLupin(cc.v2(this.lupin.node.position), 'general/crawl', 'emotion/sinister')
                            tween(this.background)
                                .delay(.5)
                                .to(2.5, {position: cc.v3(-443, 0)})
                                .start()
                        })
                        .to(3, {position: cc.v3(628, -316)})
                        .call(() => {
                            EffectManager.hideScene((node) => {
                                this.background.position = cc.v3(0, 0)
                                this.otherSprite[5].node.active = true

                                this.otherSpine[2].setAnimation(0, 'general/crawl_idle', true)
                                this.otherSpine[2].setAnimation(1, 'emotion/sinister', true)

                                EffectManager.showScene()
                            }, this.node)
                        })
                        .delay(2)
                        .call(() => {
                            this.otherSpine[2].setAnimation(1, 'emotion/fear_1', true)
                        })
                        .delay(2)
                        .call(() => {
                            this.showContinue()
                        })
                        .start()
                }, this.node)
            }
        })
    }

    runOption2(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/sinister')

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_1/teleport') {
                this.otherSpine[0].node.scale = 0
                this.otherSpine[0].node.active = true

                tween(this.otherSpine[0].node)
                    .to(.8, {scale: 1})
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/walk', true)
                        tween(this.lupin.node)
                            .delay(1)
                            .to(.5, {opacity: 0})
                            .start()
                    })
                    .delay(2)
                    .call(() => {
                        EffectManager.hideScene((node) => {
                            this.lupin.node.opacity = 255
                            this.lupin.clearTrack(1)
                            this.lupin.setAnimation(0, 'general/celebrate_ronaldo2', true)
                            this.lupin.node.position = cc.v3(201, 29)

                            this.setBGFrame(3)
                            this.background.position = cc.v3(-321, 0)

                            this.hideSpriteMainSence()

                            this.otherSpine[0].node.position = cc.v3(191, 265)

                            this.otherSpine[1].timeScale = 0
                            this.otherSpine[1].node.active = true

                            EffectManager.showScene()

                            this.scheduleOnce(() => {
                                tween(this.otherSpine[0].node)
                                    .to(.8, {scale: 0})
                                    .start()
                            }, 1)

                            this.scheduleOnce(() => {
                                this.otherSpine[1].timeScale = 1
                                this.otherSpine[1].setAnimation(0, 'level_26_2/mc_stone', false)
                            }, 3)

                            this.scheduleOnce(() => {
                                this.lupin.node.active = false
                                this.otherSprite[4].node.active = true
                            }, 3.4)

                            this.scheduleOnce(() => {
                                this.showContinue()
                            }, 5)
                        }, this.node)
                    })
                    .start()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_1/teleport', false)
        }, 1)

    }

    runOption3(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/sinister')
        this.otherSpine[3].node.active = true

        this.scheduleOnce(() => {
            tween(this.otherSpine[3].node).to(7, {position: cc.v3(-719, 345)}).start()
        }, 3)

        this.scheduleOnce(() => {
            this.otherSprite[6].node.active = true
            tween(this.otherSprite[6].node)
                .to(.5, {position: cc.v3(-5, -300)})
                .delay(2)
                .call(() => {
                    this.setLupin(cc.v2(this.lupin.node.position), 'general/win_1.1', 'emotion/sinister')
                })
                .delay(4)
                .call(() => {
                    this.onPass()
                })
                .start()
        }, 5)

        this.scheduleOnce(() => {
            this.lupin.setAnimation(1, 'emotion/whistle', true)
        }, 1)
    }
}
