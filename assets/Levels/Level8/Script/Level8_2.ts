import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    THROW,
    KNIFE,
    TIPTOE,
    KICK,
    HIT,
    SPIN,
    SWOOSH,
    HEYA,
    SCREAM
  }
@ccclass
export default class Level8_2 extends LevelBase {

    onEnable(): void {
        super.onEnable();
    }

    initStage(): void {
        super.initStage();
        tween(this.node).delay(2).call(() => {
            this.showOptionContainer(true);
        }).start();
        this.background.position = cc.v3(0, 0);
        this.lupin.node.position = cc.v3(250, -625);
        this.lupin.node.scale = 0.9;
        this.lupin.node.zIndex = cc.macro.MAX_ZINDEX;
        this.otherSprite[1].node.zIndex = cc.macro.MAX_ZINDEX - 1;
        this.otherSprite[0].node.position = cc.v3(140, -545);
        this.otherSprite[0].node.active = false;
        this.otherSprite[0].node.scaleX = 0.2;
        this.otherSprite[0].node.angle = -20;
        this.otherSprite[2].node.position = cc.v3(-155, 0);
        this.otherSprite[2].node.scale = 1;
        this.lupin.setAnimation(1, "emotion/sinister", true);
        this.lupin.setAnimation(0, "level_8/hide_after_cabinet", true);
        this.setOtherSpine(this.otherSpine[0], cc.v2(-250, -55), null, "police/level_8/sit_decorate");
    }

    runOption1(): void {
        this.otherSpine[0].setMix("police/level_8/sit_decorate", "police/level_8/sit_surprised", 0.3);
        this.otherSpine[0].setMix("police/level_8/sit_surprised", "police/level_8/nervous", 0.3);
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.otherSprite[0].node.active = false;
                this.otherSprite[2].node.scale = 1.2;
                this.otherSprite[2].node.position = cc.v3(-200, -80);
                this.otherSpine[0].node.position = cc.v3(-280, -135);
                this.otherSpine[0].node.scaleX = -0.8;
                this.otherSpine[0].node.scaleY = 0.8;
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_8/cockroach_throw", false);
                this.lupin.setAnimation(1, "emotion/sinister", true);
            })
            .to(0.5, {opacity: 0})
            .delay(1)
            .call(() => {
                this.otherSprite[0].node.active = true;
                this.playSound(SOUND.THROW, false, 0)
                tween(this.otherSprite[0].node).bezierTo(0.8, cc.v2(80, -515), cc.v2(80, 0), cc.v2(-20, -300))
                    .call(() => {
                        this.playSound(SOUND.TIPTOE, false, 1)
                        this.lupin.setAnimation(1, "emotion/excited", true);
                    })
                    .delay(0.5)
                    .to(0.5, {angle: 60})
                    .to(2, {position: cc.v3(-10, -120)})
                    .to(0.2, {angle: 90})
                    .to(1, {position: cc.v3(-36, -50)})
                    .call(() => {
                        cc.audioEngine.stopAllEffects();
                        this.playSound(SOUND.SCREAM, false, 0)
                        this.otherSprite[0].node.angle = 25;
                        this.otherSprite[0].node.scaleX = -0.2;
                        this.otherSpine[0].setAnimation(1, "police/level_8/sit_surprised", false);
                        this.otherSpine[0].addAnimation(1, "police/level_8/nervous", false);
                        this.otherSpine[0].setCompleteListener(track => {
                            if (track.animation.name == "police/level_8/nervous")
                            {
                                this.lupin.node.scaleX = -0.9;
                                this.lupin.setAnimation(0, "general/win_2.1", true);
                                this.lupin.setAnimation(1, "emotion/sinister", true);
                                tween(this.node).delay(2).call(() => {
                                    this.onPass();
                                }).start();
                            }
                        })
                    })
                    .by(1, {position: cc.v3(-100, 0)})
                    .start();
            })
            .start();
    }

    runOption2(): void {
        let isTrue = true;
        this.otherSpine[0].setMix("police/level_8/sit_decorate", "police/level_8/sit_surprised", 0.3);
        this.otherSpine[0].setMix("police/level_8/sit_surprised", "police/level_8/sit_nervous", 0.5);
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.otherSprite[2].node.scale = 1.2;
                this.otherSprite[2].node.position = cc.v3(-200, -80);
                this.lupin.node.zIndex = cc.macro.MAX_ZINDEX - 1;
                this.otherSprite[1].node.zIndex = cc.macro.MAX_ZINDEX;
                this.otherSpine[0].node.zIndex = cc.macro.MAX_ZINDEX;
                this.otherSpine[0].node.position = cc.v3(-280, -135);
                this.otherSpine[0].node.scaleX = -0.8;
                this.otherSpine[0].node.scaleY = 0.8;
                this.lupin.node.position = cc.v3(30, -525);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_8/approach_cabinet", true);
                this.playSound(SOUND.TIPTOE, false, 0)
                tween(this.lupin.node).parallel(
                        tween().to(5, {scaleX: 0.8}),
                        tween().to(5, {scaleY: 0.8}),
                        tween().by(5, {position: cc.v3(-40, 325)})
                    )
                    .call(() => {
                        cc.audioEngine.stopAllEffects()
                        this.playSound(SOUND.SWOOSH, false, 0)
                        this.playSound(SOUND.SPIN, false, 2)
                        this.scheduleOnce(()=>{
                            cc.audioEngine.stopAllEffects()
                            this.playSound(SOUND.HIT, false, 0)
                        },6)
                        this.lupin.timeScale = 1.5;
                        this.lupin.setAnimation(0, "level_8/nunchaku_", false);
                        this.lupin.addAnimation(0, "general/nunchaku2", true);
                        this.lupin.node.scaleX = -0.8;
                        this.otherSpine[0].setAnimation(1, "police/level_8/sit_surprised", false);
                        this.lupin.setStartListener(track => {
                            if (track.animation.name == "general/nunchaku2")
                            {
                                this.otherSpine[0].setAnimation(1, "police/level_8/sit_nervous", false);
                            }
                        });
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "general/nunchaku2" && isTrue)
                            {
                                isTrue = false;
                                tween(this.lupin.node).delay(3)
                                    .call(() => {
                                        this.lupin.setAnimation(0, "level_8/nunchaku3", false);
                                        this.otherSpine[0].setAnimation(1, "police/level_8/sit_decorate", false);
                                    })
                                    .delay(2)
                                    .call(() => {
                                        this.showContinue();
                                    }).start();
                            }
                        });
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption3(): void {
        this.otherSpine[0].setMix("police/level_8/sit_decorate", "police/level_8/sit_surprised", 0.3);
        this.otherSpine[0].setMix("police/level_8/sit_surprised", "police/level_8/sit_kick2", 0.3);
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.otherSprite[2].node.scale = 1.2;
                this.otherSprite[2].node.position = cc.v3(-200, -80);
                this.lupin.node.zIndex = cc.macro.MAX_ZINDEX - 1;
                this.otherSprite[1].node.zIndex = cc.macro.MAX_ZINDEX;
                this.otherSpine[0].node.zIndex = cc.macro.MAX_ZINDEX;
                this.otherSpine[0].node.position = cc.v3(-280, -135);
                this.otherSpine[0].node.scaleX = -0.8;
                this.otherSpine[0].node.scaleY = 0.8;
                this.lupin.node.position = cc.v3(50, -525);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_8/approach_cabinet", true);
                this.playSound(SOUND.TIPTOE, false, 0)
                tween(this.lupin.node).parallel(
                        tween().to(5, {scaleX: 0.82}),
                        tween().to(5, {scaleY: 0.82}),
                        tween().by(5, {position: cc.v3(0, 325)})
                    )
                    .call(() => {
                        cc.audioEngine.stopAllEffects()
                        this.playSound(SOUND.KNIFE, false, 0)
                        this.playSound(SOUND.HEYA, false, 0.5)
                        this.lupin.setAnimation(0, "general/knife_attach", false);
                        this.lupin.node.scaleX = -0.82;
                        this.otherSpine[0].setAnimation(1, "police/level_8/sit_surprised", false);
                    })
                    .delay(1)
                    .call(() => {
                        this.scheduleOnce(()=>{
                            cc.audioEngine.stopAllEffects()
                            this.playSound(SOUND.KICK, false, 0)
                        },0.5)

                        this.otherSpine[0].node.position = cc.v3(-280, -185);
                        this.otherSpine[0].setAnimation(1, "police/level_8/sit_kick2", false);
                        tween(this.lupin.node).delay(0.6).call(() => {
                            this.lupin.setAnimation(0, "general/back", false);
                            this.lupin.setAnimation(1, "emotion/fear_1", false);
                        })
                        .delay(2)
                        .call(() => {
                            this.showContinue();
                        })
                        .start();
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }
    
}
