import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";

const {ccclass, property} = cc._decorator;

const tween = cc.tween;

enum SOUND {
    ROCK,
    PUNCH,
    HEAD_BANG,
    DIZZY,
    WALL,
    ALERT
  }

@ccclass
export default class Level1 extends LevelBase {

    @property(cc.SpriteFrame)
    imgHole: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    imgCrack: cc.SpriteFrame = null;

    @property(cc.Node)
    text: cc.Node = null;

    skipIntro = false;

    onLoad(): void {
        super.onLoad();
        this.text.scale = 0.6;
        this.text.opacity = 0;
    }

    onEnable(): void {
        super.onEnable();
        this.otherSprite[0].node.position = cc.v3(402, -250, 0);
        this.scheduleOnce(() => {
            EffectManager.showScene();
        }, 0.4);
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    onDisable(): void {
        super.onDisable();

    }

    start(): void {
        super.start()
        this._gameManager.firstPlay = true;
    }

    initStage() : void {
        super.initStage();
        // this.effectText();
        this.showOptionContainer(false);
        this.otherSprite[0].spriteFrame = this.imgCrack;
        this.otherSprite[1].node.position = cc.v3(-165, 40);
        this.otherSprite[1].node.zIndex = cc.macro.MIN_ZINDEX;
        super.setLupin(cc.v2(-250, -460), "emotion/sinister", "general/stand_thinking");
        super.setOtherSprite(this.otherSprite[0], cc.v2(402, -205));
        this.otherSprite.forEach(sprite => {
            sprite.node.active = false;
        });
        this.otherSprite[0].node.active = true;
        this.lupin.node.scaleX = 1;
        this.currentId = 0;
        // this.introduce();
        this.background.position = cc.v3(0, 40);

        if (!this.skipIntro) {
            this.skipIntro = true
            this.effectText();
            this.introduce()
        } else {
            this.noIntro()
        }
    }

    effectText():void {
        tween(this.text).parallel(
            tween().to(1.5, {scale: 1.1}, {easing: "cubicIn"}),
            tween().to(1, {opacity: 255}, {easing: "cubicIn"})
        )
        .to(0.5, {scale: 1}, {easing: "cubicOut"})
        .delay(1.5)
        .parallel(
            tween().to(2, {scale: 10}, {easing: "cubicOut"}),
            tween().to(1, {opacity: 0}, {easing: "cubicOut"})
        )
        .start();
    }

    introduce(): void {
        let isTrue = true;
        this.lupin.timeScale = 1
        tween(this.lupin.node).delay(1)
            .call(() => {
                this.lupin.setAnimation(1, "general/walk", true);
                tween(this.lupin.node).by(.8, {position: cc.v3(180, 110)})
                    .call(() => {
                        this.lupin.timeScale = 1.5
                        this.lupin.setAnimation(1, "level_1/see_windows", false);
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "level_1/see_windows" && isTrue)
                            {
                                isTrue = false;
                                this.lupin.timeScale = 1
                                this.lupin.setAnimation(1, "general/walk", true);
                                tween(this.lupin.node).by(1.2, {position: cc.v3(320, -80)})
                                    .call(() => {
                                        let isTrue2 = true;
                                        this.lupin.timeScale = 1.5
                                        this.lupin.setAnimation(1, "level_1/knock_wall", false);
                                        this.lupin.setCompleteListener(track => {
                                            if (track.animation.name == "level_1/knock_wall" && isTrue2)
                                            {
                                                isTrue2 = false;
                                                this.lupin.node.scaleX = -1;
                                                this.lupin.timeScale = 1
                                                this.lupin.setAnimation(0, "emotion/sinister", true);
                                                this.lupin.setAnimation(1, "general/walk", true);
                                                tween(this.lupin.node).to(1.5, {position: cc.v3(-250, -460)})
                                                    .call(() => {
                                                        this.lupin.node.scaleX = 1;
                                                        super.setLupin(cc.v2(-250, -460), "emotion/sinister", "general/stand_thinking");
                                                        tween(this.node).delay(2).call(() => {
                                                            this.showOptionContainer(true);
                                                            this._gameManager.showGuide(true);
                                                        }).start();
                                                    }).start();
                                            }
                                        })
                                    })
                                    .start();
                            }
                        })
                    }).start();
            })
            .start();
    }

    noIntro(): void {
        this.lupin.node.scaleX = 1;
        super.setLupin(cc.v2(-250, -460), "emotion/sinister", "general/stand_thinking");
        tween(this.node).delay(2).call(() => {
            this.showOptionContainer(true);
            this._gameManager.showGuide(true);
        }).start();
    }

    runOption1() : void {
        this.lupin.setAnimation(1, "general/walk", true);
        tween(this.lupin.node).by(2, {position: cc.v3(250, 60)})
            .call(() => {
                let loop = true;
                let count = 0;
                this.lupin.timeScale = 1.5;
                this.playSound(SOUND.ROCK,false,0.3)
                this.lupin.setAnimation(1, "level_1/lv2_stg1_tray_hit", true);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_1/lv2_stg1_tray_hit" && loop)
                    {
                        ++count;
                        if (count < 5){
                            this.playSound(SOUND.ROCK,false,0.3)
                        }
                        if (count == 5)
                        {
                            loop = false;
                            this.lupin.setAnimation(1, "general/back", false);
                            this.lupin.setAnimation(0, "emotion/fear_1", true);
                            tween(this.node).delay(1)
                                .call(()=> {
                                    this.showFail(this.selected);
                                })
                                .start();
                        }
                        else if (count == 3)
                        {
                            this.playSound(SOUND.ALERT,false,0.5)
                            this.otherSprite[1].node.active = true;
                            tween(this.otherSprite[1].node).by(0.5, {position: cc.v3(100, 0)}).start();
                        }
                    }
                })
            })
            .start();
    }

    runOption3() : void {
        this.lupin.setAnimation(1, "general/walk", true);
        this.lupin.setMix("general/head_hit_1", "level_7/fall_giddy", 0.2);
        tween(this.lupin.node).by(2, {position: cc.v3(350, 0)})
            .call(() => {
                let loop = true;
                let count = 0;
                this.playSound(SOUND.HEAD_BANG,false,0.4)
                this.lupin.setAnimation(1, "general/head_hit_1", true);
                this.lupin.setAnimation(0, "emotion/angry", true);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/head_hit_1" && loop)
                    {
                        ++count;
                        if (count == 3)
                        {
                            loop = false;
                            this.playSound(SOUND.DIZZY,false,0)
                            this.lupin.setAnimation(0, "level_7/fall_giddy", false);
                            this.lupin.setAnimation(1, "general/fall", false);
                            tween(this.node).delay(1)
                                .call(()=> {
                                    this.showFail(this.selected);
                                })
                                .start();
                        }else{
                            this.playSound(SOUND.HEAD_BANG,false,0.4)
                        }
                    }
                })
            })
            .start();
    }
    
    nextScene(event: cc.Event.EventCustom): void {
        var newScene = this.checkNextSceneExits(this.currentId, this.selected);
        if (newScene)
        {
            newScene.active = true;
            this._gameManager._currentScene = newScene;
            ++this.currentId;
            return;
        }

        newScene = this.getInstantiate(this.currentId, this.selected);

        if (newScene)
        {
            newScene.zIndex = -1;
            this._gameManager._currentScene = newScene;
            this.arrScene.push(newScene);
            this.scheduleOnce(() => {
                this.node.addChild(newScene);
                this._gameManager._currentScene.parent.active = true;
            }, 0.2);
        }

        ++this.currentId;
    }
    runOption2(): void {
        this.lupin.setAnimation(0, "emotion/sinister", true);
        tween(this.lupin.node).parallel(
            tween().delay(0).to(1, {position: cc.v3(0, -460, 0)}).call(() => {
                this.lupin.addAnimation(1, "general/hand_hit", true);
                this.playSound(SOUND.PUNCH,false,1.5)

            }),
            tween().call(() => {
                this.lupin.setAnimation(1, "general/walk", false);
            })
        ).start();

        var count = 0;
        this.lupin.setCompleteListener(function(trackEntry) {
            if (trackEntry.animation.name == "general/hand_hit") {
                if (count != 2)
                {
                    this.playSound(SOUND.PUNCH,false,1.5)
                    ++count;
                    return;
                }
                this.playSound(SOUND.WALL,false,0)
                this.otherSprite[0].node.position = cc.v3(402, -310, 0);
                this.otherSprite[0].spriteFrame = this.imgHole;    
                this.lupin.setAnimation(1, "general/win_2.1", true);
                this.lupin.setAnimation(0, "emotion/happy_1", true);
                this.scheduleOnce(() => {
                    this.showSuccess(this.selected);
                }, 1);
            }
        }.bind(this));
    }
}

    