import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from '../../../Scripts/EffectManager';


const {ccclass, property} = cc._decorator
const tween = cc.tween;

@ccclass
export default class Level30_2 extends LevelBase {

    initStage(): void {
        super.initStage()
        this.setStatus()
        this.setAction()
    }

    setStatus(): void {
        this.lupin.node.active = true
        this.lupin.node.scale = .8
        this.setLupin(cc.v2(-1311, -486), 'general/walk', 'emotion/whistle')
        this.lupin.setCompleteListener(null)
        this.lupin.node.zIndex = 0

        this.background.position = cc.v3(590, 0)

        this.otherSpine[0].node.scaleX = -.8
        this.otherSpine[0].setAnimation(0, 'level_30_2/witch_dance', true)
        this.otherSpine[0].node.position = cc.v3(558, -312)

        this.otherSpine[1].node.active = false
        this.otherSpine[2].node.active = false

        this.otherSpine[3].node.active = false
        this.otherSpine[3].node.position = cc.v3(-466, -516)
        this.otherSpine[3].node.scale = 2
        this.otherSpine[3].node.zIndex = 1

        this.otherSprite[0].node.active = false

        this.otherSprite[1].node.active = false
        this.otherSprite[1].node.angle = 0
        this.otherSprite[1].node.zIndex = 1

        this.otherSprite[2].node.scale = 0
        this.otherSprite[2].node.zIndex = 2

        this.otherSprite[3].node.active = false
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(4, {position: cc.v3(137, -486)})
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/idle')
            })
            .delay(2)
            .call(() => {
                this.otherSpine[0].node.scaleX = .8
                this.otherSpine[0].setAnimation(0, 'level_30_2/witch_idle', true)
            })
            .delay(1)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_30_2/witch_angry', true)
            })
            .delay(.5)
            .call(() => {
                this.lupin.setAnimation(1, 'emotion/fear_1', true)
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.background)
            .delay(1)
            .to(3, {position: cc.v3(-495, 0)})
            .start()
    }

    runOption1(): void {
        tween(this.lupin.node)
            .delay(1)
            .flipX()
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/fear_2')
            })
            .to(1.7, {position: cc.v3(-875, -486)})
            .delay(3)
            .call(() => {
                EffectManager.hideScene(() => {
                    this.lupin.node.position = cc.v3(471, -557)

                    this.background.position = cc.v3(-440.36, 0)

                    this.otherSprite[0].node.active = true
                    this.otherSprite[1].node.active = true

                    EffectManager.showScene()

                    tween(this.lupin.node).to(2, {position: cc.v3(-433, -557)}).start()
                    tween(this.background).to(2, {position: cc.v3(537, 0)}).start()
                    tween(this.otherSprite[1].node).delay(1.5).to(.5, {angle: -90}).start()

                    tween(this.otherSprite[2].node)
                        .delay(2)
                        .to(.2, {scale: 10})
                        .call(() => {
                            this.otherSprite[1].node.active = false
                            this.lupin.node.active = false
                            this.otherSprite[3].node.active = true
                        })
                        .to(.2, {scale: 0})
                        .delay(2)
                        .call(() => {
                            this.showContinue()
                        })
                        .start()
                })
            })
            .start()

        tween(this.background)
            .delay(1)
            .to(1.5, {position: cc.v3(577, 0)})
            .to(1, {position: cc.v3(-495, 0)}, {easing: 'quadOut'})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_30_2/witch_magic_use', true)
            })
            .start()
    }

    runOption2(): void {
        this.lupin.setMix('level_22_3/treo_2', 'level_22_3/fall', .3)

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'general/run', true)
            this.lupin.node.scaleX = -.8

            tween(this.lupin.node)
                .to(2, {position: cc.v3(-518, -96)})
                .call(() => {
                    this.lupin.node.position = cc.v3(-592, -26)
                    this.lupin.setAnimation(0, 'level_22_3/treo_1', false)
                    this.lupin.setAnimation(1, 'emotion/sinister', true)
                    this.lupin.addAnimation(0, 'level_22_3/treo_2', true)
                })
                .to(3, {position: cc.v3(-614, 442)})
                .start()

            tween(this.background)
                .to(2, {position: cc.v3(581, -169)})
                .delay(2)
                .call(() => {
                    this.otherSpine[2].node.active = true
                    this.otherSpine[2].setAnimation(0, 'fx/explosion', false)
                })
                .delay(.3)
                .call(() => {
                    this.otherSpine[1].node.active = true
                    this.otherSpine[1].setAnimation(0, 'snake_attack', false)
                    this.otherSpine[1].addAnimation(0, 'snake_slither', true)
                })
                .delay(.6)
                .call(() => {
                    cc.Tween.stopAllByTarget(this.lupin.node)

                    this.lupin.clearTrack(1)
                    this.lupin.setAnimation(0, 'level_22_3/fall', true)
                    tween(this.lupin.node)
                        .to(1.3, {position: cc.v3(-347, -76)})
                        .call(() => {
                            this.lupin.setAnimation(0, 'level_22_3/faint', false)
                        })
                        .delay(1)
                        .call(() => {
                            this.showContinue()
                        })
                        .start()
                })
                .start()
        }, 1)
    }

    runOption3(): void {
        this.lupin.setMix('general/stand_nervous', 'level_28_1/goi_dien_thoai', .3)
        this.setLupin(cc.v2(this.lupin.node.position), 'level_28_1/goi_dien_thoai', 'emotion/sinister')

        this.otherSpine[0].setMix('level_30_2/witch_angry2', 'level_30_3/witch_bloom_fly', .3)

        this.otherSpine[3].node.active = true
        this.otherSpine[3].setAnimation(0, 'kong/kong_walking', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_30_2/mc_ride_kong') {
                this.lupin.node.zIndex = 2
                this.lupin.setAnimation(0, 'level_30_3/mc_riding_Kong', true)
                this.lupin.node.position = cc.v3(510, -484)

                this.otherSpine[3].node.scaleX = 2
                this.otherSpine[3].setAnimation(0, 'kong/kong_runing', true)

                tween(this.lupin.node).to(2, {position: cc.v3(1395, -484)}).start()
                tween(this.otherSpine[3].node).to(2, {position: cc.v3(1414, -493)}).start()

                this.scheduleOnce(() => {
                    this.otherSpine[0].node.scaleX = -.8
                    this.otherSpine[0].setAnimation(0, 'level_30_2/witch_angry2', false)
                }, 1)
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_30_2/witch_angry2') {
                this.otherSpine[0].setCompleteListener(null)
                this.otherSpine[0].setAnimation(0, 'level_30_3/witch_bloom_fly', true)

                tween(this.otherSpine[0].node)
                    .to(1.3, {position: cc.v3(1211, 8)})
                    .call(() => {
                        this.onPass()
                    })
                    .start()
            }
        })

        tween(this.otherSpine[3].node)
            .delay(2)
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/happy_1')
            })
            .to(3, {position: cc.v3(530, -493)})
            .flipX()
            .call(() => {
                this.otherSpine[3].setAnimation(0, 'kong/kong_take_mc_up', false)
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_30_2/mc_ride_kong', false)
                tween(this.lupin.node).delay(.5).to(.5, {position: cc.v3(92, -550)}).start()
            })
            .start()
    }
}
