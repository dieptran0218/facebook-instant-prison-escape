import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

@ccclass
export default class Level22_1 extends LevelBase {

    onEnable(): void {
        super.onEnable()
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage()
        this.setStatus()
        this.setAction()
    }

    setStatus(): void {
        this.lupin.setToSetupPose()
        this.lupin.clearTracks()
        this.lupin.node.active = true
        this.setLupin(cc.v2(-910, -650), 'level_22_1/mc_walking_cold', null)
        this.lupin.setCompleteListener(null)

        this.background.position = cc.v3(538, 0)

        this.otherSpine[0].setAnimation(0, 'bear_male/bear_idle', true)
        this.otherSpine[0].node.position = cc.v3(-872, -340)
        this.otherSpine[0].setCompleteListener(null)

        this.otherSpine[1].node.active = false
        this.otherSpine[1].setCompleteListener(null)

        this.otherSpine[2].node.active = false

        this.otherSprite[0].node.scale = .8
        this.otherSprite[0].node.position = cc.v3(626, 960)

    }

    setAction(): void {
        tween(this.lupin.node)
            .to(4, {position: cc.v3(-69, -650)})
            .call(() => {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_nervous',
                    'emotion/fear_1',
                )
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.background).to(4, {position: cc.v3(-290, 0)}).start()
    }

    runOption1(): void {

        this.scheduleOnce(() => {
            this.lupin.clearTracks()
            this.lupin.setToSetupPose()

            this.lupin.setAnimation(0, 'level_22_1/mc_werewolf_1', false)
        }, 1)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_22_1/mc_werewolf_1') {
                this.lupin.setAnimation(0, 'level_22_1/mc_werewolf_idle', true)
                this.scheduleOnce(() => {
                    this.lupin.setAnimation(0, 'level_22_1/mc_werewolf_attack', false)

                    this.scheduleOnce(() => {
                        tween(this.otherSpine[0].node).to(.3, {position: cc.v3(-1040, -542)}).start()
                        tween(this.lupin.node)
                            .to(.3, {position: cc.v3(198, -555)})
                            .call(() => {
                                this.otherSpine[2].node.active = true
                            })
                            .delay(2)
                            .call(() => {
                                this.otherSpine[2].node.active = false
                                this.lupin.setAnimation(0, 'level_22_1/mc_werewolf_die', false)
                                this.otherSpine[0].setAnimation(0, 'bear_male/bear_die', false)
                            })
                            .delay(2)
                            .call(() => {
                                this.showFail(this.selected)
                            })
                            .start()
                    }, 1.5)

                    this.scheduleOnce(() => {
                        this.otherSpine[0].setAnimation(0, 'bear_male/bear_attack', false)
                    }, 1)
                }, 2)
            }
        })

        tween(this.otherSprite[0].node)
            .to(3, {position: cc.v3(264, 844), scale: 1.5})
            .start()
    }

    runOption2(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'fx/explosion') {
                this.lupin.setCompleteListener(null)

                this.lupin.node.active = false
                
                this.otherSpine[1].node.active = true
                this.otherSpine[1].setAnimation(0, 'bear_female/f_bear_idle', true)

                this.scheduleOnce(() => {
                    this.otherSpine[1].setAnimation(0, 'bear_female/f_bear_charm', false)
                }, 1)
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'bear_male/bear_suprise') {
                this.otherSpine[0].setCompleteListener(null)
                this.otherSpine[0].setAnimation(0, 'bear_male/bear_waving_flag', false)

                this.scheduleOnce(() => {
                    this.showFail(this.selected)
                }, 2)
            }
        })

        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'bear_female/f_bear_charm') {
                this.otherSpine[1].setCompleteListener(null)

                this.otherSpine[0].setAnimation(0, 'bear_male/bear_suprise', false)
                this.otherSpine[1].setAnimation(0, 'bear_female/f_bear_idle', true)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.clearTracks()
            this.lupin.findSlot('head_status').attachment = null
            this.lupin.setAnimation(0, 'fx/explosion', false)
        }, 1)
    }

    runOption3(): void {
        this.lupin.clearTrack(1)
        this.lupin.setMix('general/stand_nervous', 'level_22_1/mc_walk_vodkar', .3)
        this.lupin.setAnimation(0, 'level_22_1/mc_walk_vodkar', true)

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'bear_male/bear_drink') {
                tween(this.lupin.node)
                    .delay(1)
                    .call(() => {
                        this.lupin.setAnimation(1, 'emotion/whistle', true)
                    })
                    .delay(2)
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/walk', true)
                    })
                    .to(2, {position: cc.v3(982, -266)})
                    .call(() => {
                        this.onPass()
                    })
                    .start()
            }
        })

        tween(this.lupin.node)
            .to(2, {position: cc.v3(283, -364)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_22_1/mc_give_vodkar', false)
            })
            .delay(.2)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'bear_male/bear_drink', false)
                this.lupin.timeScale = 0
            })
            .delay(.2)
            .call(() => {
                this.lupin.timeScale = 1
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_nervous',
                    'emotion/fear_1',
                )
            })
            .start()
    }
}
