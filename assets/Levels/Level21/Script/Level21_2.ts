import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

@ccclass
export default class Level21_2 extends LevelBase {

    private _aborigines1;
    private _aborigines2;
    private _emotion1;
    private _emotion2;
    private _snake;
    private _fx;

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }

    setStatus(): void {
        this._aborigines1 = this.otherSpine[0];
        this._aborigines2 = this.otherSpine[1];
        this._emotion1 = this.otherSpine[2];
        this._emotion2 = this.otherSpine[3];
        this._snake = this.otherSpine[4];
        this._fx = this.otherSpine[5];
        this._emotion1.node.active = false;
        this._emotion2.node.active = false;
        this._fx.node.active = false;
        this._fx.timeScale = 0.7;
        this._fx.node.scale = 1.1;
        this.lupin.setAnimation(1, "emotion/happy_1", true);
        this.lupin.clearTrack(1);
        this.lupin.node.scaleX = 0.8;
        this.setLupin(cc.v2(-700, -550), "general/walk", "emotion/idle");
        this._aborigines1.node.active = true;
        this._aborigines1.setAnimation(0, "level_21_2_soldier/soldier_1_idle", true);
        this._aborigines1.node.position = cc.v3(230, -475);
        this._aborigines2.setAnimation(0, "level_21_2_soldier/soldier_2_idle2", true);
        this._aborigines2.node.position = cc.v3(-95, -550);
        this._aborigines2.node.scaleX = -0.8;
        this._aborigines2.node.scaleY = 0.8;
        this._snake.node.scale = 0;
        this._snake.node.x = -580;
        this.lupin.setMix("general/walk", "level_8/hide_after_cabinet", 0.3);
        this._snake.setAnimation(0, "snake_slither", true);
    }

    setAction(): void {
        tween(this.lupin.node).to(2, {position: cc.v3(-445, -550)})
            .call(() => {
                this.lupin.timeScale = 1.5;
                this.lupin.setAnimation(0, "level_8/hide_after_cabinet", true);
                this.lupin.node.scaleX = -0.8;
                this.lupin.setAnimation(1, "emotion/thinking", true);
                tween(this.node).delay(2).call(() => {
                    this.showOptionContainer(true);
                }).start();
            })
            .by(0.3, {x: -80})
            .start();
    }

    boom(): void {
        this._fx.node.active = true;
        this._fx.setAnimation(0, "fx/explosion", false);
        this._fx.setCompleteListener(track => {
            if (track.animation.name == "fx/explosion") {
                this._fx.node.active = false;
            }
        })
    }
    runOption1(): void {
        this.lupin.clearTrack(1);
        this.lupin.node.opacity = 0;
        this.lupin.setAnimation(0, "level_21_2/mc_girl_idle", true);
        this.lupin.node.scaleX = 0.8;
        this.boom();
        tween(this.lupin.node).delay(0.3).to(0.5, {opacity: 255})
            .call(() => {
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_21_2/mc_girl_idle") {
                        this.lupin.setCompleteListener(null);
                        this.lupin.setAnimation(0, "level_21_2/mc_girl_walk", true);
                        tween(this.lupin.node).to(2, {position: cc.v3(-250, -620)})
                            .call(() => {
                                this.lupin.setAnimation(0, "level_21_2/mc_girl_kiss", true);
                            })
                            .start();
                        tween(this.node).delay(1)
                            .call(() => {
                                this._aborigines2.node.scaleX = 0.8;
                            })
                            .delay(2)
                            .call(() => {
                                let count = 0;
                                this._emotion1.timeScale = 0.8;
                                this._emotion2.timeScale = 0.8;
                                this._emotion1.node.active = true;
                                this._emotion2.node.active = true;
                                this._emotion1.setAnimation(0, "fx/fly_heart", false);
                                this._emotion1.addAnimation(0, "fx/angry_bubble", false);
                                this._emotion2.setAnimation(0, "fx/fly_heart", false);
                                this._emotion2.addAnimation(0, "fx/angry_bubble", false);
                                this._emotion1.setCompleteListener(track => {
                                    if (track.animation.name == "fx/fly_heart") {
                                        this._emotion1.setCompleteListener(null);
                                        this._aborigines2.node.scaleX = -0.8;
                                    }
                                })
                                this._emotion2.setCompleteListener(track => {
                                    if (track.animation.name == "fx/angry_bubble") {
                                        this._emotion2.setCompleteListener(null);
                                        this._emotion1.node.active = false;
                                        this._emotion2.node.active = false;
                                    }
                                })
                            })
                            .delay(3.5)
                            .call(() => {
                                this._aborigines1.setAnimation(0, "level_21_2_soldier/soldier_1_threaten", false);
                                this._aborigines2.setAnimation(0, "level_21_2_soldier/soldier_1_threaten", false);
                                tween(this._aborigines2.node).to(0.3, {position: cc.v3(150, -480)})
                                    .call(() => {
                                        this._aborigines1.node.active = false;
                                        this._aborigines2.node.scale = 2.5;
                                        this._aborigines2.node.position = cc.v3(100, -350);
                                        this._aborigines2.setAnimation(0, "police/general/fight_cloud", true);
                                    })
                                    .delay(1)
                                    .call(() => {
                                        this.lupin.setAnimation(0, "general/run", true);
                                        this.lupin.setAnimation(1, "emotion/sinister", true);
                                        tween(this.lupin.node).by(1, {x: 1000})
                                            .call(() => {
                                                this.onPass();
                                            })
                                            .start();
                                    }).start();
                            })
                            .start();
                    }
                })
            })
            .start();

    }

    runOption2(): void {
        this.lupin.setMix("level_7/put_cockroach", "level_8/hide_after_cabinet", 0.5);
        this._snake.setMix("snake_slither", "snake_die", 0.3);
        this.lupin.node.scaleX = 0.8;
        this.lupin.timeScale = 1.5;
        this.lupin.setAnimation(0, "level_7/put_cockroach", false);
        this._snake.node.scale = 0.2;
        tween(this._snake.node).delay(0.3)
            .to(0.25, {x: -430}, {easing: "quadIn"})
            .bezierTo(0.7, cc.v2(this._snake.node.position), cc.v2(-200, -300), cc.v2(100, -570))
            .to(0.7, {scale: 0.4})
            .call(() => {
                this._aborigines2.setAnimation(0, "level_21_2_soldier/soldier_1_attack", false);
                tween(this._snake.node).delay(0.45)
                    .call(() => {
                        this._snake.setAnimation(0, "snake_die", false);
                    })
                    .start();
            })
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(1, "emotion/fear_2", true);
            })
            .delay(1)
            .call(() => {
                this.showContinue();
            })
            .start();
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_7/put_cockroach") {
                this.lupin.setCompleteListener(null);
                this.lupin.node.scaleX = -0.8;
                this.lupin.setAnimation(0, "level_8/hide_after_cabinet", true);
            }
        })
    }

    runOption3(): void {
        this.lupin.clearTrack(1);
        this.lupin.setMix("level_8/hide_after_cabinet", "general/ghost_fake", 0.3);
        this.lupin.setAnimation(0, "general/ghost_fake", true);
        this.lupin.node.scaleX = 0.8;
        tween(this.lupin.node).to(0.6, {position: cc.v3(-400, -590)})
            .call(() => {
                this._aborigines2.node.scaleX = 0.8;
                this._aborigines1.setAnimation(0, "level_21_2_soldier/soldier_1_threaten", true);
                this._aborigines2.setAnimation(0, "level_21_2_soldier/soldier_1_threaten", true);
            })
            .delay(0.6)
            .call(() => {
                this._emotion1.setAnimation(0, "fx/angry_bubble", false);
                this._emotion1.node.active = true;
                this._emotion1.timeScale = 0.8;
                this._emotion2.setAnimation(0, "fx/angry_bubble", false);
                this._emotion2.node.active = true;
                this._emotion2.timeScale = 0.8;
                this.lupin.setAnimation(0, "level_21_2/mc_threaten", false);
                this.lupin.addAnimation(0, "general/back", false);
                this.lupin.setStartListener(track => {
                    if (track.animation.name == "general/back") {
                        this._emotion1.node.active = false;
                        this._emotion2.node.active = false;
                        this.lupin.setAnimation(0, "emotion/fear_2", true);
                        tween(this.lupin.node).delay(1)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    }
                })
            })
            .start();
    }
}
