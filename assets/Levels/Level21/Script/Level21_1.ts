import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

@ccclass
export default class Level21_1 extends LevelBase {

    @property(cc.Node)
    long: cc.Node = null;

    private _birds;
    private _chong;
    private _lock;
    private _scene;
    private _treeBranch;
    private _front;
    private _back;
    private _column;
    private _day;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }

    setStatus(): void {
        this._front = this.otherSpine[2];
        this._back = this.otherSpine[1];
        this._birds = this.otherSpine[0];
        this._chong = this.otherSprite[3];
        this._scene = this.otherSprite[4];
        this._lock = this.otherSprite[1];
        this._column = this.otherSprite[2];
        this._day = this.otherSprite[5];
        this._treeBranch = this.otherSprite[0];
        this._treeBranch.node.position = cc.v3(88, 222);
        this._treeBranch.node.angle = 0;
        this._birds.node.position = cc.v3(-1000, 560);
        this.setLupin(cc.v2(-25, -620), "general/stand_nervous", "emotion/abc");
        this._chong.node.scaleY = 0;
        this._front.node.zIndex = 1;
        this.lupin.node.zIndex = 0;
        this.long.position = cc.v3(-125, 520);
        this._lock.node.active = true;
        this._lock.node.zIndex = 1;
        this._column.node.active = true;
        this._column.node.zIndex = 1;
        this._treeBranch.node.active = true;
        this._scene.node.active = false;
        this._birds.node.scaleX = 1;
        this.long.scaleX = 1;
        this._treeBranch.node.angle = 0;
        this._treeBranch.node.zIndex = 3;
        this._day.node.active = true;
        this._day.node.zIndex = 2;
        this.lupin.node.scale = 1;
        this._front.setAnimation(0, "level_21_cage/cage_front", false);
        this._back.setAnimation(0, "level_21_cage/cage_back", false);
        this._front.node.position = cc.v3(20, -680);
        this._back.node.zIndex = -1;
    }

    setAction(): void {
        this.scheduleOnce(() => {
            this.showOptionContainer(true);
        }, 1);
    }

    runOption1(): void {
        this.lupin.clearTrack(1);
        this._front.setAnimation(0, "level_21_cage/cage_front_open", false);
        this.lupin.setAnimation(0, "level_21_1/mc_open_cage", false);
        this._lock.node.zIndex = 0;
        this.lupin.node.zIndex = 1;
        tween(this.shadow).delay(1)
            .to(0.5, {opacity: 255})
            .call(() => {
                this._front.node.zIndex = 0;
                this._lock.node.active = false;
                this._column.node.active = false;
                this.lupin.setAnimation(0, "general/walk", true);
                this.lupin.setAnimation(1, "emotion/excited", false);
                tween(this.lupin.node).to(0.5, {position: cc.v3(-20, -645)})
                    .call(() => {
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(0, "level_21_1/mc_jump_down", false);
                    })
                    .delay(0.3)
                    .to(0.2, {position: cc.v3(200, -1080)})
                    .call(() => {
                        this.lupin.setAnimation(0, "general/stand_nervous", true);
                        this.lupin.setAnimation(1, "emotion/excited", false);
                    })
                    .delay(1)
                    .call(() => {
                        this.lupin.setMix("level_21_1/mc_emoHurt", "level_21_1/mc_emoDie", 0.3);
                        tween(this._chong.node).to(0.2, {scaleY: 1}).start();
                        this.lupin.timeScale = 2;
                        this.lupin.setAnimation(0, "level_21_1/mc_emoHurt", false);
                        tween(this.lupin.node).delay(0.3)
                            .call(() => {
                                this.lupin.timeScale = 1;
                                this.lupin.clearTrack(1);
                                this.lupin.setAnimation(0, "level_21_1/mc_emoDie", false);
                                tween(this.node).delay(2)
                                    .call(() => {
                                        this.showFail(this.selected);
                                    })
                                    .start();
                            })
                            .start();
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_21_1/mc_flute", true);
        tween(this._birds.node).delay(1)
            .to(1, {position: cc.v3(-80, 665)})
            .parallel(
                tween().to(3, {scale: 0}),
                tween().to(3, {position: cc.v3(430, 900)}),
                tween().call(() => {
                    this.lupin.clearTrack(1);
                    this.lupin.setAnimation(0, "general/stand_nervous", true);
                    this.lupin.setAnimation(1, "emotion/happy_2", false);
                    tween(this.long).parallel(
                            tween().to(3, {scale: 0}),
                            tween().to(3, {position: cc.v3(430, 900)}),
                        )
                        .start();
                })
            )
            .start();
        tween(this.shadow).delay(5)
            .to(0.5, {opacity: 255})
            .call(() => {
                this._scene.node.active = true;
                tween(this._birds.node).parallel(
                        tween().to(3, {position: cc.v3(-10, 200)})
                            .delay(1)
                            .call(() => {
                                tween(this._treeBranch.node).parallel(
                                    tween().to(0.5, {angle: -30}, {easing: "cubicIn"})
                                        .call(() => {
                                            this._day.node.active = false;
                                            this.lupin.setAnimation(0, "general/walk", true);
                                            this.lupin.setAnimation(1, "emotion/worry", false);
                                            tween(this.lupin.node).by(0.5, {x: 90})
                                                .call(() => {
                                                    this.lupin.clearTrack(1);
                                                    this.lupin.setAnimation(0, "level_21_1/mc_shake_cage", true);
                                                })
                                                .delay(1.5)
                                                .call(() => {
                                                    this.lupin.setAnimation(0, "level_21_1/mc_cry", false);
                                                })
                                                .delay(1.5)
                                                .call(() => {
                                                    this.showFail(this.selected);
                                                })
                                                .start();
                                        }),
                                    tween().to(0.5, {position: cc.v3(88, -50)}, {easing: "cubicIn"})
                                )
                                .start();
                            })
                            .to(1, {position: cc.v3(-1200, 480)}),
                        tween().to(3, {scaleY: 1}),
                        tween().to(3, {scaleX: -1}),
                        tween().call(() => {
                            tween(this.long).to(3, {position: cc.v3(-10, 90)}).start();
                            tween(this.long).to(3, {scaleY: 1}).start();
                            tween(this.long).to(3, {scaleX: -1}).start();
                        })
                    )
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption3(): void {
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_21_1/mc_hulk1", false);
        this.lupin.addAnimation(0, "level_21_1/mc_hulk2", false);
        this.lupin.addAnimation(0, "level_21_1/mc_hulk3", false);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_21_1/mc_hulk1") {
                tween(this.lupin.node).call(() => {
                        this._lock.node.active = false;
                        this._column.node.active = false;
                        this.scheduleOnce(() => {
                            this._day.node.active = false;
                        }, 0.4);
                        this._front.setAnimation(0, "level_21_cage/cage_crack_front", false);
                        this._back.setAnimation(0, "level_21_cage/cage_crack_back", false);
                        tween(this._front.node).delay(0.3).by(0.5, {y: -350}, {easing: "cubicIn"}).start();
                        tween(this.lupin.node).delay(0.3).by(0.7, {y: -500}, {easing: "cubicIn"})
                            .call(() => {
                                this.lupin.setAnimation(0, "level_21_1/mcHulk_landing", false);
                                this.lupin.node.zIndex = 2;
                            })
                            .delay(1)
                            .call(() => {
                                this.lupin.setAnimation(0, "level_21_1/mcHulk_run", true);
                            })
                            .by(1, {x: 1000})
                            .call(() => {
                                this.onPass();
                            })
                            .start();
                    })
                    .parallel(
                        tween().to(0.5, {scale: 1.3}),
                        tween().by(0.5, {y: 100})
                    )
                    .start();
                    
            }
        })
    }
}
