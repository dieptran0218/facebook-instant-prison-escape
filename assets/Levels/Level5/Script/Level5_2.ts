import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";

enum SOUND {
    THROW,
    HIT,
    ALERT,
    SAX,
    SLIP,
    SNEAK,
    BEEP,
    SWITCH
  }

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
@ccclass
export default class Level5_2 extends LevelBase {

    @property(cc.Node)
    powerOff: cc.Node = null;

    onLoad(): void {
        super.onLoad();
    }

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
    }

    initStage(): void {
        super.initStage();
        this.setLupin(cc.v2(-420, -460), null, "general/hide1");
        this.setOtherSpine(this.otherSpine[0], cc.v2(390, -460), null, "police/level_5/stand_watch");
        this.lupin.node.active = true;
        this.otherSpine[0].node.active = true;
        this.otherSprite.forEach(sprite => {
            if (sprite.node.name != "PowerOff") {
                sprite.node.active = true;
            }
        });
        this.setCamera(this.camera2d[0], cc.v2(0, 0), 1.0);
        this.otherSprite[3].node.active = false;
        tween(this.node).delay(2).call(() => {
            this.showOptionContainer(true);
        }).start();
    }

    runOption1(): void {
        tween(this.node).delay(1)
                .call(() => {
                    this.remoteControl();
                })
                .start();
    }

    remoteControl(): void {
        this.lupin.setAnimation(0, "emotion/excited", false);
        this.lupin.setAnimation(1, "level_5/remote_control", false);
        this.playSound(SOUND.BEEP, false, 0)
        tween(this.shadow).delay(1).to(0.2, {opacity: 255})
                    .delay(0.5)
                    .call(() => {
                        this.playSound(SOUND.SAX, false, 0)
                        this.otherSprite[0].node.active = false;
                        this.setOtherSpine(this.otherSpine[0], cc.v2(230, -410), null, "police/level_5/stand_watch");
                        this.camera2d[0].active = true;
                        this.camera2d[1].active = true;
                        this.otherSprite[3].node.active = true;
                        this.setCamera(this.camera2d[0], cc.v2(0, -200), 1.3);
                        this.walk();

                })
                .to(0.2, {opacity: 0})
                .start();
    }

    walk(): void {
        this.setLupin(cc.v2(-680, -460), "emotion/sinister", "general/walk");
                        tween(this.lupin.node).by(6, {position: cc.v3(1200, 0)})
                                    .call(() => {
                                        this.showSuccess(this.selected);
                                    })
                                    .start();
    }

    runOption2(): void {
        this.isTrue = true;
        tween(this.node).delay(1)
                .call(() => {
                    this.lupin.clearTracks();
                    this.lupin.setAnimation(0, "emotion/sinister", false);
                    this.lupin.setCompleteListener(trackEntry => {
                        if (trackEntry.animation.name == "emotion/sinister" && this.isTrue) {
                            this.isTrue = false;
                            this.throwBida();
                        }
                    });
                })
                .start();
    }

    throwBida(): void {
        this.lupin.setAnimation(1, "emotion/worry", false);
        this.lupin.setAnimation(0, "level_5/throw_bida", false);
        this.playSound(SOUND.THROW, false, 0.5)
        this.lupin.setCompleteListener(trackEntry => {
            if (trackEntry.animation.name == "level_5/throw_bida") {
                this.playSound(SOUND.HIT, false, 0)
                this.otherSpine[0].setAnimation(1, "police/level_5/under_hit", false);
                this.otherSpine[0].setCompleteListener(trackEntry => {
                    if (trackEntry.animation.name == "police/level_5/under_hit") {
                        this.playSound(SOUND.ALERT, false, 0)
                        this.lupin.setAnimation(0, "emotion/fear_2", true);
                        this.lupin.setAnimation(1, "general/back", true);
                        tween(this.node).call(() => {
                                this.detected();
                            })
                            .start();
                    }
                });
            }
        })
    }

    detected(): void {
        this.otherSpine[0].setAnimation(1, "detect", false);
        this.otherSpine[0].setCompleteListener(trackEntry => {
            if (trackEntry.animation.name == "detect")
            {
                tween(this.node).call(() => {
                    this.showContinue();
                })
                .start();
            }
        })
    }

    runOption3(): void {
        this.lupin.setMix("general/walk_slow_2", "general/fall_sml", 0.3);
        tween(this.node).call(() => {
                            this.walkSlow();
                        })
                        .start();
    }

    walkSlow(): void {
        this.otherSprite[2].node.active = true;
        this.setLupin(cc.v2(this.lupin.node.position), null, "general/walk_slow_2");
        this.otherSprite[2].node.active = true;
        this.otherSprite[0].node.active = false;
        this.playSound(SOUND.SWITCH, false, 0)
        this.playSound(SOUND.SNEAK, false, 0.5)
        tween(this.lupin.node).by(3, {position: cc.v3(250, 0)})
                        .call(() => {
                            this.camera2d[0].active = true;
                            this.camera2d[1].active = true;
                            this.playSound(SOUND.SWITCH, false, 0)
                            this.setCamera(this.camera2d[0], cc.v2(150, -200), 1.5);
                            tween(this.lupin.node).by(2, {position: cc.v3(50, 0)}).start();
                            tween(this.otherSprite[2].node).repeat(2,
                                    tween().to(0.1, {opacity: 0}).delay(0.2).to(0.1, {opacity: 255}).delay(0.2)
                            )
                            .call(() => {
                                this.playSound(SOUND.SLIP, false, 0)
                                this.playSound(SOUND.ALERT, false, 0.5)
                                this.fallSml();
                            }).start();
                        })
                        .start();
    }

    fallSml(): void {
        this.otherSprite[2].node.active = false;
        this.lupin.setAnimation(1, "general/fall_sml", false);
        this.lupin.setAnimation(0, "emotion/fear_2", true);
        tween(this.node).call(() => {
            this.detected();
        })
        .start();
    }
}
