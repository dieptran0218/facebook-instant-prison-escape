import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    NO_SMOKING,
    BRAWL,
    SWOOSH,
    BRUTE_LAUNCH
  }

@ccclass
export default class Level5_1 extends LevelBase {

    onLoad(): void {
        super.onLoad();
    }

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    runOption1(): void {
        this.isTrue = true;
        tween(this.lupin.node).delay(1)
                        .call(() => {
                            this.setLupin(cc.v2(-275, -340), "emotion/sad", "general/walk");
                            tween(this.lupin.node).by(2, {position: cc.v3(375, 0)})
                                            .call(() => {
                                                this.setLupin(cc.v2(100, -340), "emotion/fear_1", "level_5/give_cigarete");
                                                this.lupin.setCompleteListener(this.listenerComplete.bind(this));
                                            })
                                            .start();
                        })
                        .start();                   
    }

    listenerComplete(trackEntry): void {
        if (trackEntry.animation.name == "level_5/give_cigarete" && this.isTrue) {
            this.playSound(SOUND.NO_SMOKING, false, 0)
            this.isTrue = false;
            this.otherSpine[0].setAnimation(1, "jailbirds/level_5/stand_threaten_cigaret", false);
            this.lupin.setAnimation(0, "emotion/fear_2", true);
            this.lupin.setAnimation(1, "general/back", false);
            this.otherSpine[0].setCompleteListener((trackEntry) => {
                if (trackEntry.animation.name == "jailbirds/level_5/stand_threaten_cigaret") {
                    this.stopAction();
                    this.showFail(this.selected);
                    this.lupin.timeScale = 0;
                }
            })
        }
        else if (trackEntry.animation.name == "jailbirds/level_5/hit" && this.isTrue) {
            this.isTrue = false;
            this.lupin.addAnimation(1, "fx/fightcloud", true);
            tween(this.otherSpine[0].node).delay(0.5)
                        .call(() => {
                            this.otherSpine[0].node.active = false;
                        }).start();
            tween(this.lupin.node).delay(3)
                            .call(() => {
                                this.lupin.setAnimation(1, "general/fall", false);
                                this.lupin.setAnimation(0, "level_7/fall_giddy", true);
                                this.otherSpine[0].setAnimation(1, "jailbirds/level_5/fall_star", false);
                                this.otherSpine[0].node.active = true;
                                this.lupin.node.position = cc.v3(50, -340);
                                this.otherSpine[0].setCompleteListener(this.listenerComplete.bind(this));
                            })
                            .start();
        }
        else if (trackEntry.animation.name == "jailbirds/level_5/fall_star") {
            this.lupin.timeScale = 0;
            this.stopAction();
            this.showFail(this.selected);
        }
        else if (trackEntry.animation.name == "level_5/give_phone") {
            this.playSound(SOUND.SWOOSH, false, 0)
            this.playSound(SOUND.BRUTE_LAUNCH, false, 2)

            this.otherSpine[0].setAnimation(1, "jailbirds/level_5/get_phone", false);
            this.otherSpine[0].setCompleteListener(this.listenerComplete.bind(this));
            tween(this.lupin.node).delay(0.4)
                            .call(() => {
                                this.lupin.setAnimation(0, "emotion/fear_2", true);
                                this.lupin.setAnimation(1, "general/back", false);
                                this.lupin.setCompleteListener(this.listenerComplete.bind(this));
                            })
                            .start();

        }
        else if (trackEntry.animation.name == "emotion/fear_2") {
            this.lupin.setCompleteListener(null);
            console.log("%cComplete general/back", "color: blue");
            this.setLupin(cc.v2(this.lupin.node.position), "emotion/excited", "level_5/walk_free");
            tween(this.lupin.node).by(4, {position: cc.v3(650, 0)})
                            .call(() => {
                                this.onPass();
                            })
                            .start();
        }
    }

    runOption2(): void {
        this.isTrue = true;
        tween(this.node).delay(1)
                    .call(() => {
                        this.lupin.setAnimation(1, "general/run", true);
                        this.lupin.setAnimation(0, "emotion/angry", true);
                        tween(this.lupin.node).by(0.5, {position: cc.v3(375, 0)})
                                        .call(() => {
                                            this.playSound(SOUND.BRAWL, false, 0)
                                            this.otherSpine[0].setAnimation(1, "jailbirds/level_5/hit", true);
                                            this.lupin.setAnimation(1, "general/kick2", false);
                                            this.otherSpine[0].setCompleteListener(this.listenerComplete.bind(this));
                                        })
                                        .start();
                    })
                    .start();
    }

    runOption3(): void {
        this.isTrue = true;
        tween(this.node).delay(1)
                    .call(() => {
                        this.setLupin(cc.v2(-275, -340), "emotion/thinking", "general/walk");
                        tween(this.lupin.node).by(2, {position: cc.v3(305, 0)})
                                        .call(() => {
                                            this.lupin.setAnimation(0, "emotion/fear_1", true);
                                            this.lupin.setAnimation(1, "level_5/give_phone", false);
                                            this.lupin.setCompleteListener(this.listenerComplete.bind(this));
                                        })
                                        .start();
                    })
                    .start();
    }

    //Override
    initStage(): void {
        super.initStage();
        this.isTrue = true;
        this.setLupin(cc.v2(-275, -340), "emotion/fear_1", "general/stand_nervous");
        this.setOtherSpine(this.otherSpine[0], cc.v2(275, -340), null, "jailbirds/level_5/stand_threaten");
        this.lupin.node.active = true;
        this.otherSpine[0].node.active = true;
        this.otherSprite[0].node.active = true;
        this.otherSprite[1].node.active = true;
        tween(this.node).delay(2).call(() => {
            this.showOptionContainer(true);
        }).start();
        this.setBeginSceen(this.node);
    }
}
