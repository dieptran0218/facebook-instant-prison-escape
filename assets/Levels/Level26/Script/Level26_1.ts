import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from '../../../Scripts/EffectManager';


const {ccclass, property} = cc._decorator
const tween = cc.tween;

@ccclass
export default class Level26_1 extends LevelBase {

    onEnable(): void {
        super.onEnable()
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage()
        this.setStatus()
        this.setAction()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-737, -490), 'general/walk', 'emotion/tired')
        this.lupin.node.scale = 1
        this.lupin.timeScale = 1
        this.lupin.setCompleteListener(null)

        this.background.color = cc.color(150, 150, 150, 255)

        this.otherSpine[0].node.position = cc.v3(682, -528)
        this.otherSpine[0].setAnimation(0, 'snake_slither', true)

        this.otherSpine[1].node.position = cc.v3(708, -444)
        this.otherSpine[1].setAnimation(0, 'snake_slither', true)
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(3, {position: cc.v3(0, -490)})
            .call(() => {
                this.lupin.setAnimation(0, 'general/stand_nervous', true)
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        EffectManager.hideScene((node) => {
            this.lupin.clearTrack(1)
            this.lupin.setAnimation(0, 'level_26_1/sit', true)

            this.otherSpine[1].node.position = cc.v3(708, -501)

            EffectManager.showScene()

            tween(this.otherSpine[1].node)
                .delay(1)
                .to(7, {position: cc.v3(-691, -501)})
                .call(() => {
                    tween(this.background)
                        .to(.8, {color: cc.color(255, 255, 255, 255)})
                        .start()
                })
                .delay(2)
                .call(() => {
                    this.setLupin(
                        cc.v2(this.lupin.node.position),
                        'general/celebrate_ronaldo2',
                        'emotion/happy_2',
                    )
                })
                .delay(2)
                .call(() => {
                    this.onPass()
                })
                .start()
        }, this.node)
    }

    runOption2(): void {
        this.lupin.setMix('level_26_1/tree_fall', 'level_26_1/tree_fall2', .3)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_9/door_creep') {
                EffectManager.hideScene((node) => {
                    this.setLupin(cc.v2(-280, 430.032), 'level_26_1/tree_sleep', null)

                    EffectManager.showScene()

                    tween(this.lupin.node)
                        .delay(2)
                        .call(() => {
                            tween(this.background)
                                .by(0.05, {position: cc.v3(0, 10)})
                                .by(0.05, {position: cc.v3(0, -20)})
                                .call(() => {
                                    this.lupin.setAnimation(0, 'level_26_1/tree_fall', false)
                                })
                                .by(0.05, {position: cc.v3(0, 10)})
                                .by(0.05, {position: cc.v3(0, 10)})
                                .by(0.05, {position: cc.v3(0, -20)})
                                .by(0.05, {position: cc.v3(0, 10)})
                                .start()
                        })
                        .start()
                }, this.node)
            }

            if (track.animation.name === 'level_26_1/tree_fall') {
                this.lupin.setCompleteListener(null)
                tween(this.lupin.node)
                    .to(.8, {position: cc.v3(-280, -406)}, {easing: 'cubicIn'})
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_26_1/tree_fall2', false)
                    })
                    .delay(2)
                    .call(() => {
                        this.showFail(this.selected)
                    })
                    .start()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.node.scaleX = -1
            this.setLupin(cc.v2(this.lupin.node.position), 'general/walk', 'emotion/sinister')

            tween(this.lupin.node)
                .to(2, {position: cc.v3(-447, -363), scaleX: -.8, scaleY: .8})
                .call(() => {
                    this.lupin.clearTrack(1)
                    this.lupin.setAnimation(0, 'level_9/door_creep', false)
                })
                .start()
        }, 1)
    }

    runOption3(): void {
        this.lupin.setMix('level_26_1/ground_sleep', 'level_26_1/tree_fall', .3)
        this.lupin.setMix('level_26_1/tree_fall', 'level_26_1/face_hurt', .3)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_26_1/tree_fall') {
                this.lupin.setCompleteListener(null)
                this.lupin.timeScale = 1
                this.lupin.setAnimation(0, 'level_26_1/face_hurt', true)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.clearTrack(1)
            this.lupin.setAnimation(0, 'level_26_1/ground_sleep', true)

            tween(this.otherSpine[0].node)
                .delay(2)
                .to(3, {position: cc.v3(0, -528)})
                .call(() => {
                    this.otherSpine[0].setAnimation(0, 'snake_attack', true)
                })
                .delay(1)
                .call(() => {
                    this.lupin.timeScale = .7
                    this.lupin.setAnimation(0, 'level_26_1/tree_fall', false)
                })
                .delay(2)
                .call(() => {
                    this.showFail(this.selected)
                })
                .start()

            tween(this.otherSpine[1].node)
                .delay(2.5)
                .to(3, {position: cc.v3(94, -444)})
                .call(() => {
                    this.otherSpine[1].setAnimation(0, 'snake_attack', true)
                })
                .start()
        }, 1)
    }
}
