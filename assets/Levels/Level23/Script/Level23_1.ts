import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

@ccclass
export default class Level23_1 extends LevelBase {

    private _aborigines;
    private _fx;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }

    setStatus(): void {
        this._aborigines = this.otherSpine[0];
        this._fx = this.otherSpine[1];
        this._fx.node.active = false;
        this._fx.node.position = cc.v3(-80, -350);
        this.setOtherSpine(this._aborigines, cc.v2(435, -245), "level_21_2_soldier/soldier_1_idle", null);
        this._aborigines.setMix("level_21_2_soldier/soldier_1_idle", "level_21_2_soldier/soldier_1_threaten", 0.3);
        this._aborigines.setMix("level_21_2_soldier/soldier_1_threaten", "level_21_2_soldier/soldier_1_idle", 0.3);
        this._aborigines.setMix("level_23_2/nativesoldier_angry", "level_21_2_soldier/soldier_1_idle", 0.3);
        // this._aborigines.setMix("level_21_2_soldier/soldier_1_threaten", "level_23_2/nativesoldier_heart", 0.3);
        this._aborigines.setMix("level_23_2/nativesoldier_heart", "level_23_2/nativesoldier_angry", 0.3);
        this.setLupin(cc.v2(-700, -350), "general/walk", "emotion/abc");
        this.lupin.setMix("general/walk", "general/stand_nervous", 0.3);
        this.lupin.node.scale = 1;
        this._aborigines.node.active = true;
    }

    setAction(): void {
        tween(this.lupin.node).by(4, {x: 620})
            .call(() => {
                this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_threaten", true);
                this.lupin.setAnimation(0, "general/stand_nervous", true);
                this.lupin.setAnimation(1, "emotion/fear_1", true);
            })
            .start();
        tween(this.camera2d[0]).by(4, {x: 700})
            .delay(2)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
    }

    runOption1(): void {
        this.lupin.setAnimation(0, "general/run", true);
        this.lupin.setAnimation(1, "emotion/angry", false);
        tween(this.lupin.node).to(0.7, {position: cc.v3(210, -245)})
            .call(() => {
                this._aborigines.node.active = false;
                this.lupin.setAnimation(1, "emotion/excited", false);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "fx/fightcloud", true);
                this.lupin.node.scale = 1.3;
                this.lupin.node.position = cc.v3(350, -50)
            })
            .delay(3)
            .call(() => {
                this.lupin.node.scaleX = -1;
                this.lupin.node.scaleY = 1;
                this.lupin.node.y = -250;
                this.lupin.setAnimation(0, "general/stand_thinking", false);
                this.lupin.setAnimation(1, "emotion/laugh", true);
                this._aborigines.node.active = true;
                this._aborigines.setAnimation(0, "level_23_1/soldier_run", true);
                tween(this._aborigines.node).by(2.5, {x: -1500})
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            })
            .start();
    }

    runOption2(): void {
        this._fx.node.active = true;
        this._fx.setAnimation(0, "fx/explosion", false);
        this._fx.setCompleteListener(track => {
            if (track.animation.name == "fx/explosion") {
                this._fx.setCompleteListener(null);
                this._fx.node.active = false;
            }
        })
        tween(this.lupin.node).to(0.3, {opacity: 0})
            .call(() => {
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_21_2/mc_girl_idle", false);
                this.lupin.addAnimation(0, "level_21_2/mc_girl_kiss", false);
                this.lupin.addAnimation(0, "level_21_2/mc_girl_walk", false);
                this.lupin.setStartListener(track => {
                    if (track.animation.name == "level_21_2/mc_girl_walk") {
                        tween(this.lupin.node).by(1, {position: cc.v3(50, -50)})
                            .call(() => {
                                this.lupin.setAnimation(0, "level_23_1/mc_girl_fail", false);
                                this.lupin.addAnimation(0, "level_23_1/mc_girl_fail_idle", false);
                            })
                            .delay(4)
                            .call(() => {
                                this.showFail(this.selected);
                            })
                            .start();
                    }
                    else if (track.animation.name == "level_21_2/mc_girl_kiss") {
                        this._aborigines.setAnimation(0, "level_23_2/nativesoldier_heart", true);
                    }
                    else if (track.animation.name == "level_23_1/mc_girl_fail_idle") {
                        this._aborigines.setAnimation(0, "level_23_2/nativesoldier_angry", true);
                    }
                })
            })
            .to(0.3, {opacity: 255})
            .start();
    }

    runOption3(): void {
        this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_idle", true);
        this.lupin.setAnimation(0, "level_23_1/mc_sleep_spray_walk", true);
        this.lupin.setAnimation(1, "emotion/sinister", false);
        tween(this.lupin.node).to(2, {position: cc.v3(120, -245)})
            .call(() => {
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_23_1/mc_sleep_spray", false);
                this.lupin.addAnimation(0, "level_23_1/mc_sleep_spray", false);
                this.lupin.addAnimation(0, "level_23_1/mc_sleep_spray", false);
                this.lupin.addAnimation(0, "level_23_1/mc_fall_sleep", false);
                this.lupin.addAnimation(0, "level_23_1/mc_fall_sleep_idle2", true);
                this.lupin.setStartListener(track => {
                    if (track.animation.name == "level_23_1/mc_fall_sleep") {
                        this.lupin.setStartListener(null);
                        this.lupin.timeScale = 0.8;
                        this._aborigines.setAnimation(0, "level_23_2/nativesoldier_angry", true);
                        this.lupin.timeScale = 1.5;
                    }
                })
            })
            .delay(5)
            .call(() => {
                this.showFail(this.selected);
            })
            .start();
    }
}
